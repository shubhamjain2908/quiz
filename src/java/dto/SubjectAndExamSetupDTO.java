/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author SHUBHAM
 */
public class SubjectAndExamSetupDTO 
{ 
    //SubjectRegistration
    private String SubjectCode;
    private String SubjectName;
    //QuizSetup
    private String SetupID;
    private String ExamName;
    private String level;
    private String TotalQuestion;
    private String MarksForEach;
    private String NegativeMarks;
    private String Time;
    private String Author;
    //
    private String Score;
    private String Correct;
    private String Wrong;
    //QuestionInsertion or Updation
    private String Question;
    private String OptionA;
    private String OptionB;
    private String OptionC;
    private String OptionD;
    private String CorrectAnswer;
    //
    private String AnswerAttempted;    
    private String Id;

    public String getSubjectCode() {
        return SubjectCode;
    }

    public void setSubjectCode(String SubjectCode) {
        this.SubjectCode = SubjectCode;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String SubjectName) {
        this.SubjectName = SubjectName;
    }

    public String getSetupID() {
        return SetupID;
    }

    public void setSetupID(String SetupID) {
        this.SetupID = SetupID;
    }

    public String getExamName() {
        return ExamName;
    }

    public void setExamName(String ExamName) {
        this.ExamName = ExamName;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
    
    public String getTotalQuestion() {
        return TotalQuestion;
    }

    public void setTotalQuestion(String TotalQuestion) {
        this.TotalQuestion = TotalQuestion;
    }

    public String getMarksForEach() {
        return MarksForEach;
    }

    public void setMarksForEach(String MarksForEach) {
        this.MarksForEach = MarksForEach;
    }

    public String getNegativeMarks() {
        return NegativeMarks;
    }

    public void setNegativeMarks(String NegativeMarks) {
        this.NegativeMarks = NegativeMarks;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String Time) {
        this.Time = Time;
    }

    public String getScore() {
        return Score;
    }

    public String getCorrect() {
        return Correct;
    }

    public void setCorrect(String Correct) {
        this.Correct = Correct;
    }

    public String getWrong() {
        return Wrong;
    }

    public void setWrong(String Wrong) {
        this.Wrong = Wrong;
    }

    
    public void setScore(String Score) {
        this.Score = Score;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String Question) {
        this.Question = Question;
    }

    public String getOptionA() {
        return OptionA;
    }

    public void setOptionA(String OptionA) {
        this.OptionA = OptionA;
    }

    public String getOptionB() {
        return OptionB;
    }

    public void setOptionB(String OptionB) {
        this.OptionB = OptionB;
    }

    public String getOptionC() {
        return OptionC;
    }

    public void setOptionC(String OptionC) {
        this.OptionC = OptionC;
    }

    public String getOptionD() {
        return OptionD;
    }

    public void setOptionD(String OptionD) {
        this.OptionD = OptionD;
    }

    public String getCorrectAnswer() {
        return CorrectAnswer;
    }

    public void setCorrectAnswer(String CorrectAnswer) {
        this.CorrectAnswer = CorrectAnswer;
    }

    public String getAnswerAttempted() {
        return AnswerAttempted;
    }

    public void setAnswerAttempted(String AnswerAttempted) {
        this.AnswerAttempted = AnswerAttempted;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String Author) {
        this.Author = Author;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }
    
}
