/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.DBConnector;
import dto.AdminDTO;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AdminAuthenticator;

/**
 *
 * @author SHUBHAM
 */
public class AdminPassChange extends HttpServlet 
{
    HttpSession session;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.sendRedirect("index.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String username=request.getParameter("username");
        String password=request.getParameter("password");
        String newpassword=request.getParameter("newpassword");
         
        AdminDTO faculty=new AdminDTO();
        faculty.setUsername(username);
        faculty.setPassword(password);
        
        AdminAuthenticator Flogin=new AdminAuthenticator();
        boolean isLogin=Flogin.isAdmin(faculty);
        
        if(isLogin) 
        {
            try
        {
            Statement st=DBConnector.getStatement();
            String query="UPDATE admin SET password='"+newpassword+"' WHERE USERNAME='"+username+"'";
            int i=st.executeUpdate(query);
            if(i>0)
            {
                System.out.println("Password Updated...");
                //session.invalidate();
                response.sendRedirect("FirstP.jsp");
                //response.sendRedirect("facultyHome.jsp");
            }
        }
        catch(SQLException e)
        {
            System.out.println(e);
        }
        }
        else
        {
            response.sendRedirect("adminlogin.html");
        }
    }    
}
