/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dto.UserDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.LoginAuthenticator;

/**
 *
 * @author SHUBHAM
 */
public class LoginChecker extends HttpServlet 
{  
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.sendRedirect("login.html");
    }   
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String enrollment=request.getParameter("enrollment");
        String password=request.getParameter("password");
        String generatedPassword=null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(password.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
            }
        catch (Exception e)
            {
            e.printStackTrace();
            }
        UserDTO user= new UserDTO();
        user.setEnrollment(enrollment);
        user.setPassword(generatedPassword);
        
        LoginAuthenticator login=new LoginAuthenticator();
        boolean isLogin=login.isLogin(user);
        
        if(isLogin)
        {
            System.out.println("Login Successfull...");
            HttpSession session=request.getSession(true);
            session.setAttribute("enrollment",enrollment);
            RequestDispatcher rd=request.getRequestDispatcher("home.jsp");
            rd.forward(request, response);
        }
    }

}
