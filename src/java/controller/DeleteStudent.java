/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dto.SubjectAndExamSetupDTO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.EditDAO;

/**
 *
 * @author SHUBHAM
 */
public class DeleteStudent extends HttpServlet 
{
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.sendRedirect("adminHome.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        PrintWriter out=response.getWriter();
        String selected=request.getParameter("isSelectedStudent");
        if(selected.equals("checked"))
        {
        String []student=request.getParameterValues("deleteS");
        for(int i=0;i<student.length;i++)
            {
		String st = student[i]; 
                
		SubjectAndExamSetupDTO stud = new SubjectAndExamSetupDTO();
		stud.setId(st);
                
		EditDAO mgr = new EditDAO();
		boolean s=mgr.deleteStudent(stud);
                if(s)
                {
                    System.out.println("Deleted Successfully...");
                }
            } 
        
            response.sendRedirect("adminHome.jsp");
        } 
        else
        {
            out.println("Select atleast one element");
        }
    }
    
}