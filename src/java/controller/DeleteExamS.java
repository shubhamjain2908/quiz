/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dto.SubjectAndExamSetupDTO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.EditDAO;

/**
 *
 * @author SHUBHAM
 */
public class DeleteExamS extends HttpServlet 
{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.sendRedirect("FirstP.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        PrintWriter out=response.getWriter();
        String []setupid = request.getParameterValues("delete");
        String about= request.getParameter("submit");
        String selected=request.getParameter("isSelected");
        if(selected.equals("checked"))
        {
        if(about.equals("del")){
            for(int i=0;i<setupid.length;i++)
            {
		String sCode = setupid[i]; 
                
		SubjectAndExamSetupDTO subject = new SubjectAndExamSetupDTO();
		subject.setSetupID(sCode);
                
		EditDAO mgr = new EditDAO();
		boolean s=mgr.deleteSetup(subject);
                if(s)
                {
                    System.out.println("Deleted Successfully...");
                }
            } 
        }
        
        if(about.equals("off")){
            for(int i=0;i<setupid.length;i++)
            {
		String sCode = setupid[i]; 
                
		SubjectAndExamSetupDTO subject = new SubjectAndExamSetupDTO();
		subject.setSetupID(sCode);
                
		EditDAO mgr = new EditDAO();
		boolean s=mgr.deactiveExam(subject);
                if(s)
                {
                    System.out.println("Deactivated Successfully...");
                }
            } 
        }
            response.sendRedirect("facultyHome.jsp");              
    }
        else
        {
            out.println("Select atleast one element");
        }
    }
}