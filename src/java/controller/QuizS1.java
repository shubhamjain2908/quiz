/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.DBConnector;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collections;

/**
 *
 * @author SHUBHAM
 */
public class QuizS1 extends HttpServlet 
{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.sendRedirect("login.html");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String enrollment=request.getParameter("enrollment");
        String eid=request.getParameter("examname");
        String scode=request.getParameter("scode");
        String code=request.getParameter("acode");
        String tablecode="";
        int time=0,totalquestion=0,marks=0,negative=0;
        String ename="",facultyname="",level="";
        ArrayList <Integer> qid=new ArrayList <Integer>();
        ArrayList <Integer> qid1=new ArrayList <Integer>();
            try
            {
                Statement st=DBConnector.getStatement();
                ResultSet rs1=null;
                String query="Select * from quizsetup where setupid='"+eid+"'";
                ResultSet rs=st.executeQuery(query);
                while(rs.next())
                {
                    tablecode=rs.getString(9);
                    time=Integer.parseInt(rs.getString(6));
                    totalquestion=Integer.parseInt(rs.getString(3));
                    marks=Integer.parseInt(rs.getString(4));
                    negative=Integer.parseInt(rs.getString(5));
                    ename=rs.getString(2);
                    facultyname=rs.getString(7);
                    level=rs.getString(10);
                }
                if(level.equals("Random"))
                {
                    String query1="Select Qid from questions where Subject_code='"+scode+"'";
                    rs1=st.executeQuery(query1);
                }
                else
                {
                    String query1="Select Qid from questions where Subject_code='"+scode+"' AND level='"+level+"'";
                    rs1=st.executeQuery(query1);
                }
                
                while(rs1.next())
                {
                    qid.add(rs1.getInt(1));
                }
                Collections.shuffle(qid);
                for(int i=0;i<totalquestion;i++)
                {
                    qid1.add(qid.get(i));
                }
            }
            catch(Exception e)
            {
                System.out.println(e);
            }
            if (code.equals(tablecode)&& code!=null && !code.trim().equals("")) 
            {
            HttpSession session=request.getSession(true);
            session.setAttribute("enrollment",enrollment);
            session.setAttribute("examname",eid);  //setupid
            session.setAttribute("scode",scode);
            session.setAttribute("time", time);
            session.setAttribute("totalquestion", totalquestion);
            session.setAttribute("marks", marks);
            session.setAttribute("negative", negative);
            session.setAttribute("ename", ename);  //setup name
            session.setAttribute("question", qid1);
            RequestDispatcher rd=request.getRequestDispatcher("quizst2.jsp");
            rd.forward(request, response);
            }
            
            else
            {
                response.sendRedirect("FirstP.jsp");
            }
    }
    
}