/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dto.SubjectAndExamSetupDTO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.ExamSetupAuthenticator;

/**
 *
 * @author SHUBHAM
 */
public class ExamSetupChecker extends HttpServlet 
{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.sendRedirect("facultyHome.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session=request.getSession(true);
        String facultyid=(String)session.getAttribute("facultyid");
        String author=request.getParameter("fname");
        String subjectcode=request.getParameter("subject");
        String examname=request.getParameter("examname");
        String level=request.getParameter("level");
        String totalquestion=request.getParameter("totalquestion");
        String marksforeach=request.getParameter("marksforeach");
        String negativemarks=request.getParameter("negativemarks");
        String time=request.getParameter("timeduration");
        
        SubjectAndExamSetupDTO s=new SubjectAndExamSetupDTO();
        s.setAuthor(author);
        s.setSubjectCode(subjectcode);
        s.setExamName(examname);
        s.setLevel(level);
        s.setTotalQuestion(totalquestion);
        s.setMarksForEach(marksforeach);
        s.setNegativeMarks(negativemarks);
        s.setTime(time);
        s.setId(facultyid);
        
        ExamSetupAuthenticator es=new ExamSetupAuthenticator();
        boolean isRegister=es.isRegister(s);
        
        if(isRegister)
        {
            System.out.println("Exam Setup successfully...");
            //RequestDispatcher rd=request.getRequestDispatcher("facultyHome.jsp");
            //rd.forward(request, response);
            response.sendRedirect("facultyHome.jsp");
        }
        else
        {
            response.sendError(0, "unsucessfull");
        }
    }

}