/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dto.FacultyDTO;
import dto.UserDTO;
import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utility.ExcelReport;
import utility.DownloadReport;

/**
 *
 * @author SHUBHAM
 */
public class Report extends HttpServlet 
{   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.sendRedirect("FirstP.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String about=request.getParameter("about");
        ExcelReport er=new ExcelReport();
        System.gc(); 
        
        if(about.equals("complete"))
        {
            String user=request.getParameter("user");
            //System.out.println(user);
            FacultyDTO d=new FacultyDTO();
            System.gc();
            d.setFacultyid(user);

            File excelFile=er.isReportGenerated(d);

            if(excelFile.length() != 0)
            {
                DownloadReport.generateDownload(request, response, excelFile);
            }
            else
            {
                response.sendRedirect("FirstP.jsp");
            }
        }
        
        if(about.equals("branch"))
        {
            String branch=request.getParameter("branchSet");
            UserDTO u=new UserDTO();
            System.gc();
            u.setBranch(branch);
            
            File excelFile=er.isBranchReport(u);
            
            if(excelFile.length() != 0)
            {
                DownloadReport.generateDownload(request, response, excelFile);
            }
            else
            {
                response.sendRedirect("FirstP.jsp");
            }
        }
        
        if(about.equals("enrollment"))
        {
            String enrollment=request.getParameter("enrollment");
            UserDTO u=new UserDTO();
            u.setEnrollment(enrollment);
            File excelFile=er.isEnrollmentReport(u);
            
            if(excelFile.length() != 0)
            {
                DownloadReport.generateDownload(request, response, excelFile);
            }
            else
            {
                response.sendRedirect("FirstP.jsp");
            }
        }
    }
    
}