package controller;

import dto.AdminDTO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AdminAuthenticator;

/**
 *
 * @author SHUBHAM
 */
public class AdminChecker extends HttpServlet 
{

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.sendRedirect("adminlogin.html");
    } 
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String username=request.getParameter("username");
        String password=request.getParameter("password");
        
        AdminDTO admin=new AdminDTO();
        admin.setUsername(username);
        admin.setPassword(password);
        
        AdminAuthenticator login=new AdminAuthenticator();
        boolean isAdmin=login.isAdmin(admin);
        
        if(isAdmin)
        {
            System.out.println("Login Successfull...");
            HttpSession session=request.getSession(true);
            session.setAttribute("username",username);
            RequestDispatcher rd=request.getRequestDispatcher("adminHome.jsp");
            rd.forward(request, response);
        }
        else
        {
            response.sendRedirect("adminlogin.html");
        }
    }
}
