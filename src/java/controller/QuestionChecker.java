/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dto.SubjectAndExamSetupDTO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.QuestionAuthenticator;

/**
 *
 * @author SHUBHAM
 */
public class QuestionChecker extends HttpServlet 
{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.sendRedirect("facultyHome.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String qid=request.getParameter("qid");
        
        String question=request.getParameter("question");
        String optionA=request.getParameter("optionA");
        String optionB=request.getParameter("optionB");
        String optionC=request.getParameter("optionC");
        String optionD=request.getParameter("optionD");
        String answer=request.getParameter("ans");
        
        SubjectAndExamSetupDTO s=new SubjectAndExamSetupDTO();
        
        s.setQuestion(question);
        s.setOptionA(optionA);
        s.setOptionB(optionB);
        s.setOptionC(optionC);
        s.setOptionD(optionD);
        s.setCorrectAnswer(answer);
        
        QuestionAuthenticator sa=new QuestionAuthenticator();
        
        if (qid != null) {
            s.setId(qid);
            
            boolean ie=sa.isEdited(s);
            
            if(ie)
            {
                System.out.println("Successfully Edited");
                response.sendRedirect("facultyHome.jsp");
            }
            else
            {
                System.out.println("Editing failed");
                response.sendError(0, "Question Not Inserted");
            }
        }
        else {
        HttpSession session=request.getSession(true);
        String facultyid=(String)session.getAttribute("facultyid");
        String subjectcode=request.getParameter("subject");
        String level=request.getParameter("level");
        
        //s.setSubjectName(subjectname);
        s.setSubjectCode(subjectcode);
        s.setId(facultyid);
        s.setLevel(level);
        
        boolean isRegister=sa.isRegister(s);
        
        if(isRegister)
        {
            System.out.println("Successfully Inserted");
            response.sendRedirect("facultyHome.jsp");
        }
        else
        {
            response.sendError(0, "Question Not Inserted");
        }
    }
    }

}