/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dto.SubjectAndExamSetupDTO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.SubjectAuthenticator;

/**
 *
 * @author SHUBHAM
 */
public class SubjectChecker extends HttpServlet 
{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.sendRedirect("facultyHome.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session=request.getSession(true);
        String facultyid=(String)session.getAttribute("facultyid");        
        String subjectcode=request.getParameter("subjectcode");
        String subjectname=request.getParameter("subjectname");
        
        SubjectAndExamSetupDTO s=new SubjectAndExamSetupDTO();
        s.setSubjectCode(subjectcode);
        s.setSubjectName(subjectname);
        s.setId(facultyid);
        
        SubjectAuthenticator sa=new SubjectAuthenticator();
        boolean isRegister=sa.isRegister(s);
        
        if(isRegister)
        {
            System.out.println("Successfully");
            response.sendRedirect("facultyHome.jsp");
        }
        else
        {
            request.setAttribute("Error","Not an Authorised faculty!");
            response.sendRedirect("facultyHome.jsp");
            //response.sendError(0, "Subject Not Register");
        }
    }

}