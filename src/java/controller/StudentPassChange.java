/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.DBConnector;
import dto.UserDTO;
import java.io.IOException;
import java.security.MessageDigest;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.LoginAuthenticator;

/**
 *
 * @author SHUBHAM
 */
public class StudentPassChange extends HttpServlet 
{
    HttpSession session;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.sendRedirect("index.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String enrollment=request.getParameter("enrollment");
        String password=request.getParameter("password");
        String newpassword=request.getParameter("newpassword");
        String GeneratedPassword=null,NewGeneratedPassword=null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            MessageDigest md1 = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(password.getBytes());
            md1.update(newpassword.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            byte[] bytes1 = md1.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            StringBuilder sb1 = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));;
            }
            for(int i=0; i< bytes1.length ;i++)
            {
                sb1.append(Integer.toString((bytes1[i] & 0xff) + 0x100, 16).substring(1));;
            }
            //Get complete hashed password in hex format
            GeneratedPassword = sb.toString();
            NewGeneratedPassword=sb1.toString();
            //System.out.println(GeneratedPassword+"   "+NewGeneratedPassword);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
         
        UserDTO user=new UserDTO();
        user.setEnrollment(enrollment);
        user.setPassword(GeneratedPassword);
        
        LoginAuthenticator login=new LoginAuthenticator();
        boolean isLogin=login.isLogin(user);
        
        if(isLogin) 
        {
            try
        {
            Statement st=DBConnector.getStatement();
            String query="UPDATE newuser SET password='"+NewGeneratedPassword+"' WHERE password='"+GeneratedPassword+"' AND enrollment='"+enrollment+"'";
            int i=st.executeUpdate(query);
            if(i>0)
            {
                System.out.println("Password Updated...");
                //session.invalidate();
                response.sendRedirect("FirstP.jsp");
                //response.sendRedirect("facultyHome.jsp");
            }
        }
        catch(SQLException e)
        {
            System.out.println(e);
        }
        }
        else
        {
            System.out.println("Failed..");
            response.sendRedirect("facultylogin.html");
        }
    }    
}
