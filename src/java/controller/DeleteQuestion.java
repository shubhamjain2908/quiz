/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dto.SubjectAndExamSetupDTO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.EditDAO;

/**
 *
 * @author SHUBHAM
 */
public class DeleteQuestion extends HttpServlet 
{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.sendRedirect("adminHome.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String []qid = request.getParameterValues("delete");
            for(int i=0;i<qid.length;i++)
            {
		String id = qid[i]; 
                
		SubjectAndExamSetupDTO subject = new SubjectAndExamSetupDTO();
		subject.setId(id);
                
		EditDAO mgr = new EditDAO();
		boolean s=mgr.deleteQuestion(subject);
                if(s)
                {
                    System.out.println("Deleted Successfully...");
                }
            } 
            response.sendRedirect("facultyHome.jsp");              
    }
    
}