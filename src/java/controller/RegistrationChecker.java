package controller;

import dto.UserDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.RegistrationAuthenticator;

/**
 *
 * @author SHUBHAM
 */
public class RegistrationChecker extends HttpServlet 
{
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.sendRedirect("studentregistration.html");
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String name=request.getParameter("name");
        String enrollment=request.getParameter("enrollment");
        String email=request.getParameter("email");
        String pn=request.getParameter("pno");
        String date=request.getParameter("date");
        String gen=request.getParameter("sex");
        String branch=request.getParameter("branch");
        String sess=request.getParameter("sess");
        String password=request.getParameter("password");
        String generatedPassword=null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(password.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        UserDTO user=new UserDTO();
        user.setName(name);
        user.setEnrollment(enrollment);
        user.setEmail(email);
        user.setPno(pn);
        user.setDate(date);
        user.setGen(gen);
        user.setBranch(branch);
        user.setSess(sess);
        user.setPassword(generatedPassword);
        
        RegistrationAuthenticator Alogin=new RegistrationAuthenticator();
        boolean isRegister=Alogin.isRegister(user);
        
        if(isRegister) 
        {
            System.out.println("Registration Successfull...");
            HttpSession session=request.getSession(true);
            session.setAttribute("enrollment", enrollment);
            //response.sendRedirect("home.jsp");
            RequestDispatcher rd=request.getRequestDispatcher("home.jsp");
            rd.forward(request, response);
        }
        else
        {
            response.sendRedirect("studentregistration.html");
        }
    }
}
