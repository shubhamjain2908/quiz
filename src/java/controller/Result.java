/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dto.SubjectAndExamSetupDTO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.ResultDAO;

/**
 *
 * @author SHUBHAM
 */
public class Result extends HttpServlet 
{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.sendRedirect("FirstP.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session=request.getSession();
        String enrollment=(String)session.getAttribute("enrollment");
        String score = request.getParameter("score");
        String correct = request.getParameter("correct");
        String wrong = request.getParameter("wrong");
        String eid = request.getParameter("eid");
        System.out.println(score + " "+ enrollment+" "+correct +" "+wrong +" "+ eid);
        
        SubjectAndExamSetupDTO quiz=new SubjectAndExamSetupDTO();
        quiz.setId(enrollment);
        quiz.setScore(score);
        quiz.setCorrect(correct);
        quiz.setWrong(wrong);
        quiz.setSetupID(eid);
        
        ResultDAO r=new ResultDAO();
        boolean result=r.isResult(quiz);
        
        if(result)
        {
            System.out.println("Result saved.");
            RequestDispatcher rd=request.getRequestDispatcher("home.jsp");
            rd.forward(request, response);
        }
    }
    
}