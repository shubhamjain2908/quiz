/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.DBConnector;
import dto.FacultyDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utility.AuthCodeGenerator;

/**
 *
 * @author SHUBHAM
 */
public class AuthCode extends HttpServlet 
{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.sendRedirect("FirstP.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String id=request.getParameter("acId");
        String email=request.getParameter("email");
        
        FacultyDTO d=new FacultyDTO();
        d.setFacultyid(id);
        d.setEmail(email);
        AuthCodeGenerator a=new AuthCodeGenerator();
        
        boolean isG=a.isCodeGeneated(d);
        
        if(isG)
        {
            response.sendRedirect("facultyHome.jsp");
        }
        else
        {
            response.sendRedirect("FirstP.jsp");
        }
    }
    
}