/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dto.SubjectAndExamSetupDTO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.EditDAO;

/**
 *
 * @author SHUBHAM
 */
public class DeleteSubject extends HttpServlet 
{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.sendRedirect("adminHome.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        PrintWriter out=response.getWriter();
        String selected=request.getParameter("isSelectedSubject");
        if(selected.equals("checked"))
        {
            String []subjectCode = request.getParameterValues("delete");
            for(int i=0;i<subjectCode.length;i++)
            {
		String sCode = subjectCode[i]; 
                
		SubjectAndExamSetupDTO subject = new SubjectAndExamSetupDTO();
		subject.setSubjectCode(sCode);
                
		EditDAO mgr = new EditDAO();
		boolean s=mgr.deleteSubject(subject);
                if(s)
                {
                    System.out.println("Deleted Successfully...");
                }
            } 
            response.sendRedirect("adminHome.jsp");
        }
        else
        {
            out.println("<h1>Select at least One subject<h1>");
        }
    }
}