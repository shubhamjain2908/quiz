/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import db.DBConnector;
import dto.SubjectAndExamSetupDTO;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author SHUBHAM
 */
public class ExamSetupAuthenticator 
{
    public boolean isRegister(SubjectAndExamSetupDTO s)
    {
        String facultyid=s.getId();
        String author=s.getAuthor();
        String subjectcode=s.getSubjectCode();
        String examname=s.getExamName().toUpperCase();
        String level=s.getLevel();
        String totalquestion=s.getTotalQuestion();
        String marksforeach=s.getMarksForEach();
        String negativemarks=s.getNegativeMarks();
        String time=s.getTime();
        int f=0;long id=0;
        try
        {
            Statement st=DBConnector.getStatement();
            ResultSet rs1=st.executeQuery("select setupid from quizsetup order by setupid DESC limit 1 ");
                if(rs1.next())
                   id=rs1.getLong(1)+1;             
                else
                   id=1;
            String q="Select flag from faculty where facultyid='"+facultyid+"'";
            ResultSet rs=st.executeQuery(q);
            while(rs.next())
            {
                f=rs.getInt(1);
            }
        if(f>0){    
            String query="Insert into quizsetup values('"+subjectcode+"','"+examname+"','"+totalquestion+"','"+marksforeach+"','"+negativemarks+"','"+time+"','"+author+"','"+id+"','null','"+level+"')";
            System.out.println(query);
            int i=st.executeUpdate(query);
            if(i>0)
            {
                System.out.println("Exam setup Successfully");
                return true;
            }
            else
            {
                System.out.println("Failed");
            }
        }
            else
            {
                System.out.println("Not an Authorised Faculty..");
            }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        return false;
    }
}
