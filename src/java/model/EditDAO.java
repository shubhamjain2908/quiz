/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import db.DBConnector;
import dto.SubjectAndExamSetupDTO;
import java.sql.Statement;

/**
 *
 * @author SHUBHAM
 */
public class EditDAO 
{
    public boolean deleteSubject(SubjectAndExamSetupDTO se)
    {
        String sCode=se.getSubjectCode();
	try 
            {
            Statement st=DBConnector.getStatement();
            String sql = "DELETE FROM addsubject WHERE Subject_Code='"+sCode+"'";
            //System.out.println(sql);
            int i=st.executeUpdate(sql);
            String sql1="Delete From questions where Subject_Code='"+sCode+"'";
            //System.out.println(sql1);
            int j=st.executeUpdate(sql1);
                if(i>0)
                {
                    return true;
                }
            } 
	catch (Exception e) 
            {
                System.out.println(e);
                return false;
            }
        return false;
    }
    public boolean deleteSetup(SubjectAndExamSetupDTO se)
    {
        String sCode=se.getSetupID();
	try 
            {
            Statement st=DBConnector.getStatement();
            String sql = "DELETE FROM quizsetup WHERE setupid='"+sCode+"'";
            //System.out.println(sql);
            int i=st.executeUpdate(sql);
                if(i>0)
                {
                    return true;
                }
            } 
	catch (Exception e) 
            {
                e.printStackTrace();
                return false;
            }
        return false;
    }
    public boolean deleteQuestion(SubjectAndExamSetupDTO se)
    {
        String id=se.getId();
	try 
            {
            Statement st=DBConnector.getStatement();
            String sql = "DELETE FROM questions WHERE Qid='"+id+"'";
            //System.out.println(sql);
            int i=st.executeUpdate(sql);
                if(i>0)
                {
                    return true;
                }
            } 
	catch (Exception e) 
            {
                e.printStackTrace();
                return false;
            }
        return false;
    }
    public boolean deleteStudent(SubjectAndExamSetupDTO se)
    {
        String id=se.getId();
	try 
            {
            Statement st=DBConnector.getStatement();
            String sql = "DELETE FROM newuser WHERE Enrollment='"+id+"'";
            //System.out.println(sql);
            int i=st.executeUpdate(sql);
                if(i>0)
                {
                    return true;
                }
            } 
	catch (Exception e) 
            {
                e.printStackTrace();
                return false;
            }
        return false;
    }
    
    public boolean deactiveExam(SubjectAndExamSetupDTO se)
    {
        String id=se.getSetupID();
	try 
            {
            Statement st=DBConnector.getStatement();
            String sql=("update quizsetup set authcode='null' where setupid='"+id+"'");
            int i=st.executeUpdate(sql);
                if(i>0)
                {
                    return true;
                }
            } 
	catch (Exception e) 
            {
                e.printStackTrace();
                return false;
            }
        return false;
    }
}
