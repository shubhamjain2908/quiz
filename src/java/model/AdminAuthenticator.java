package model;

import db.DBConnector;
import dto.AdminDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author SHUBHAM
 */
public class AdminAuthenticator 
{
    public boolean isAdmin(AdminDTO user)
    {
        String username=user.getUsername();
        String password=user.getPassword();
        String tablePassword="";
        
        try
        {
            Statement st=DBConnector.getStatement();
            String query="Select password from admin where username='"+username+"'";
            System.out.println(query);
            ResultSet rs=st.executeQuery(query);
            if(rs.next())
            {
                tablePassword=rs.getString(1);
            }
        }
        catch(SQLException e)
        {
            System.out.println(e);
        }
        if(username!=null && password!=null && !username.trim().equals("") && !password.trim().equals("") && password.equals(tablePassword)) 
        {
            return true;
        }        
        return false;
    }
}
