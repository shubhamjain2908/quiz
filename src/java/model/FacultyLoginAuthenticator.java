/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import db.DBConnector;
import dto.FacultyDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author SHUBHAM
 */
public class FacultyLoginAuthenticator 
{
 public boolean isLogin(FacultyDTO user)
    {
        String facultyid=user.getFacultyid();
        String password=user.getPassword();
        String tablePassword="";
        
        try
        {
            Statement st=DBConnector.getStatement();
            String query="Select password from faculty where facultyid='"+facultyid+"'";
            System.out.println(query);
            ResultSet rs=st.executeQuery(query);
            if(rs.next())
            {
                tablePassword=rs.getString(1);
            }
        }
        catch(SQLException e)
        {
            System.out.println(e);
        }
        if(facultyid!=null && password!=null && !facultyid.trim().equals("") && !password.trim().equals("") && password.equals(tablePassword)) 
        {
            return true;
        }        
        return false;
    }   
}
