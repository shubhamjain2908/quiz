package model;

import db.DBConnector;
import dto.UserDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author SHUBHAM
 */
public class LoginAuthenticator 
{
    public boolean isLogin(UserDTO user)
    {
        String enrollment=user.getEnrollment();
        String password=user.getPassword();
        String tablePassword="";
        
        try
        {
            Statement st=DBConnector.getStatement();
            String query="Select password from newUser where enrollment='"+enrollment+"'";
            System.out.println(query);
            ResultSet rs=st.executeQuery(query);
            if(rs.next())
            {
                tablePassword=rs.getString(1);
            }
        }
        catch(SQLException e)
        {
            System.out.println(e);
        }
        if(enrollment!=null && password!=null && !enrollment.trim().equals("") && !password.trim().equals("") && password.equals(tablePassword)) 
        {
            return true;
        }        
        return false;
    }
}
