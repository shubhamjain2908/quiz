/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import db.DBConnector;
import dto.SubjectAndExamSetupDTO;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author SHUBHAM
 */
public class QuestionAuthenticator 
{
    Statement st=DBConnector.getStatement();
    public boolean isRegister(SubjectAndExamSetupDTO s)
    {
        String facultyid=s.getId();
        //String subjectname=s.getSubjectName();
        String subjectcode=s.getSubjectCode();
        String level=s.getLevel();
        String question=s.getQuestion();
        String optionA=s.getOptionA();
        String optionB=s.getOptionB();
        String optionC=s.getOptionC();
        String optionD=s.getOptionD();
        String answer=s.getCorrectAnswer();
        long id=0;
        int f=0;
        try
        {
            
            String q="Select flag from faculty where facultyid='"+facultyid+"'";
            ResultSet rs=st.executeQuery(q);
            while(rs.next())
            {
                f=rs.getInt(1);
                System.out.println(f);
            }
        if(f>0){
            ResultSet rs1=st.executeQuery("select Qid from questions order by Qid DESC limit 1 ");
                if(rs1.next())
                   id=rs1.getLong(1)+1;             
                else
                   id=1;
                
            String query="insert into questions values('"+id+"','"+subjectcode+"','"+question+"','"+optionA+"','"+optionB+"','"+optionC+"','"+optionD+"','"+answer+"','"+facultyid+"','"+level+"')";
            System.out.println(query);
            int i=st.executeUpdate(query);
            if(i>0)
            {
                System.out.println("Question Inserted");
                return true;
            }
            else
            {
                System.out.println("Insertion failed");
            }
        }
            else
            {
                System.out.println("Not an Authorised faculty...");
            }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        return false;
    }
    
    public boolean isEdited(SubjectAndExamSetupDTO s)
    {
        String id=s.getId();
        String question=s.getQuestion();
        String optionA=s.getOptionA();
        String optionB=s.getOptionB();
        String optionC=s.getOptionC();
        String optionD=s.getOptionD();
        String answer=s.getCorrectAnswer();
        
        try
        {
            int i=st.executeUpdate("Update questions set question='"+question+"', optiona='"+optionA+"',optionb='"+optionB+"',optionc='"+optionC+"',optiond='"+optionD+"',answer='"+answer+"' where qid='"+id+"' ");
            if(i>0)
            {
                System.out.println("Question Editedd");
                return true;
            }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        return false;
    }
}
