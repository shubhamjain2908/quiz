/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import db.DBConnector;
import dto.FacultyDTO;
import dto.UserDTO;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author SHUBHAM
 */
public class ExcelReport 
{
    Statement st=DBConnector.getStatement();
    ResultSet rs=null,rs1=null;
    Set sTree=new TreeSet();
    ArrayList<String> a=new ArrayList<>();
    ArrayList<String> b=new ArrayList<>();
    ArrayList<String> c=new ArrayList<>();
    ArrayList<String> dd=new ArrayList<>();
    ArrayList<String> e=new ArrayList<>();
    ArrayList<String> eid=new ArrayList<>();
    
    static int i=0;
    
    public File isReportGenerated(FacultyDTO d)
    {
        String faculty=d.getFacultyid();
        try
        {
            File excelFile = File.createTempFile("report", ".xlxs");
            rs=st.executeQuery("select * from result");
            
            XSSFWorkbook workbook=new XSSFWorkbook();
            XSSFSheet sheet=workbook.createSheet("result");
            XSSFRow row,row1;
            XSSFCell cell,cell1;
            
            row=sheet.createRow(1);
            cell=row.createCell(1);
                cell.setCellValue("Exam ID");
            cell=row.createCell(2);
                cell.setCellValue("Enrollment");
            cell=row.createCell(3);
                cell.setCellValue("Score");
            cell=row.createCell(4);
                cell.setCellValue("Correct");
            cell=row.createCell(5);
                cell.setCellValue("Wrong");
            cell=row.createCell(6);
                cell.setCellValue("Exam Name");
            cell=row.createCell(7);
                cell.setCellValue("Subject Code");
                
            i=2;
            while(rs.next())
            {
                sTree.add(rs.getString(2));
                
                row=sheet.createRow(i);
                cell=row.createCell(1);
                    cell.setCellValue(rs.getString(2));
                cell=row.createCell(2);
                    cell.setCellValue(rs.getString(3));
                cell=row.createCell(3);
                    cell.setCellValue(rs.getInt(4));
                cell=row.createCell(4);
                    cell.setCellValue(rs.getInt(5));
                cell=row.createCell(5);
                    cell.setCellValue(rs.getInt(6));
                cell=row.createCell(6);
                    cell.setCellValue(rs.getString(7));
                cell=row.createCell(7);
                    cell.setCellValue(rs.getString(8));
                i++;    
            }
            
            //**********************Setup Info*********************************************************//
            Iterator it=sTree.iterator();
            while(it.hasNext())
            {
                rs1=st.executeQuery("Select * from quizsetup where setupid='"+it.next()+"'");
                while(rs1.next())
                {
                    eid.add(rs1.getString(8));
                    a.add(rs1.getString(3));
                    b.add(rs1.getString(4));
                    c.add(rs1.getString(5));
                    dd.add(rs1.getString(6));
                    e.add(rs1.getString(7));
                }
            }
            
            row1=sheet.createRow(i+5);
            
            cell1=row1.createCell(1);
                cell1.setCellValue("Exam ID");
            cell1=row1.createCell(2);
                cell1.setCellValue("Total Questions");
            cell1=row1.createCell(3);
                cell1.setCellValue("Mark for each");
            cell1=row1.createCell(4);
                cell1.setCellValue("Neative Marks");
            cell1=row1.createCell(5);
                cell1.setCellValue("Time");
            cell1=row1.createCell(6);
                cell1.setCellValue("Faculty");    
                
            int j=i+6;
            for(int k=0; k<eid.size();k++)
            {
                row=sheet.createRow(j);
                
                cell=row.createCell(1);
                    cell.setCellValue(eid.get(k));
                cell=row.createCell(2);
                    cell.setCellValue(a.get(k));
                cell=row.createCell(3);
                    cell.setCellValue(b.get(k));
                cell=row.createCell(4);
                    cell.setCellValue(c.get(k));
                cell=row.createCell(5);
                    cell.setCellValue(dd.get(k));
                cell=row.createCell(6);
                    cell.setCellValue(e.get(k));
                j++;
            }
            //**********************************Setup Info**********************************//
            
            try (FileOutputStream fos = new FileOutputStream(excelFile)) 
            {
                workbook.write(fos);
            }
            
            System.out.println("Report Generated");
            return excelFile;
        }
        catch(IOException | SQLException ex)
        {
            System.out.println(ex);
        }
        return null;
    }
    
    public File isBranchReport(UserDTO u)
    {
        String branch=u.getBranch();
        ArrayList<String> enroll=new ArrayList<>();
        ArrayList<String> ed=new ArrayList<>();
        ArrayList<String> en1=new ArrayList<>();
        ArrayList<Integer> score=new ArrayList<>();
        ArrayList<Integer> correct=new ArrayList<>();
        ArrayList<Integer> wrong=new ArrayList<>();
        ArrayList<String> ename=new ArrayList<>();
        ArrayList<String> subcode=new ArrayList<>();
        
        XSSFWorkbook workbook=new XSSFWorkbook();
        XSSFSheet sheet=workbook.createSheet("result");
        XSSFRow row,row1;
        XSSFCell cell,cell1;
            
        try
        {
            File excelFile = File.createTempFile("report", ".xlxs");
            ResultSet rsB=st.executeQuery("Select enrollment from newuser where branch='"+branch+"'");
            while(rsB.next())
            {
                enroll.add(rsB.getString(1));
            }
            
            row=sheet.createRow(1);
            cell=row.createCell(1);
                cell.setCellValue("Exam ID");
            cell=row.createCell(2);
                cell.setCellValue("Enrollment");
            cell=row.createCell(3);
                cell.setCellValue("Score");
            cell=row.createCell(4);
                cell.setCellValue("Correct");
            cell=row.createCell(5);
                cell.setCellValue("Wrong");
            cell=row.createCell(6);
                cell.setCellValue("Exam Name");
            cell=row.createCell(7);
                cell.setCellValue("Subject Code");
                
            for (int j = 0; j < enroll.size(); j++) 
            {
                String query="Select * from result where enrollment='"+enroll.get(j)+"'";
                System.out.println(query);
                ResultSet rsBg=st.executeQuery(query);
                int h=2;
                while(rsBg.next())
                {
                    sTree.add(rsBg.getString(2));
                    /*xxxxxxxxxxxxxxxxxxxx*/
                    ed.add(rsBg.getString(2));
                    en1.add(rsBg.getString(3));
                    score.add(rsBg.getInt(4));
                    correct.add(rsBg.getInt(5));
                    wrong.add(rsBg.getInt(6));
                    ename.add(rsBg.getString(7));
                    subcode.add(rsBg.getString(8));
                }
            }
            int h=2;
            for (int k = 0; k < ed.size(); k++) 
            {
                row=sheet.createRow(h);
                    cell=row.createCell(1);
                        cell.setCellValue(ed.get(k));
                    cell=row.createCell(2);
                        cell.setCellValue(en1.get(k));
                    cell=row.createCell(3);
                        cell.setCellValue(score.get(k));
                    cell=row.createCell(4);
                        cell.setCellValue(correct.get(k));
                    cell=row.createCell(5);
                        cell.setCellValue(wrong.get(k));
                    cell=row.createCell(6);
                        cell.setCellValue(ename.get(k));
                    cell=row.createCell(7);
                        cell.setCellValue(subcode.get(k));
                    h++; 
            }
            
            //**********************Setup Info*********************************************************//
            Iterator it=sTree.iterator();
            while(it.hasNext())
            {
                rs1=st.executeQuery("Select * from quizsetup where setupid='"+it.next()+"'");
                while(rs1.next())
                {
                    eid.add(rs1.getString(8));
                    a.add(rs1.getString(3));
                    b.add(rs1.getString(4));
                    c.add(rs1.getString(5));
                    dd.add(rs1.getString(6));
                    e.add(rs1.getString(7));
                }
            }
            
            row1=sheet.createRow(h+5);
            
            cell1=row1.createCell(1);
                cell1.setCellValue("Exam ID");
            cell1=row1.createCell(2);
                cell1.setCellValue("Total Questions");
            cell1=row1.createCell(3);
                cell1.setCellValue("Mark for each");
            cell1=row1.createCell(4);
                cell1.setCellValue("Neative Marks");
            cell1=row1.createCell(5);
                cell1.setCellValue("Time");
            cell1=row1.createCell(6);
                cell1.setCellValue("Faculty");    
                
            int j=h+6;
            for(int k=0; k<eid.size();k++)
            {
                row=sheet.createRow(j);
                
                cell=row.createCell(1);
                    cell.setCellValue(eid.get(k));
                cell=row.createCell(2);
                    cell.setCellValue(a.get(k));
                cell=row.createCell(3);
                    cell.setCellValue(b.get(k));
                cell=row.createCell(4);
                    cell.setCellValue(c.get(k));
                cell=row.createCell(5);
                    cell.setCellValue(dd.get(k));
                cell=row.createCell(6);
                    cell.setCellValue(e.get(k));
                j++;
            }
            //**********************************Setup Info**********************************//
            
            try (FileOutputStream fos = new FileOutputStream(excelFile)) 
            {
                workbook.write(fos);
            }
            
            System.out.println("Report Generated");
            return excelFile;
        }
        
        catch(IOException | SQLException ex)
        {
            System.out.println(ex);
        }
        return null;
    }
    
    public File isEnrollmentReport(UserDTO u)
    {
        String enrollment=u.getEnrollment();
        try
        {
            File excelFile = File.createTempFile("report", ".xlxs");
            ResultSet rsE=st.executeQuery("Select * from result where enrollment='"+enrollment+"'");
            
            XSSFWorkbook workbook=new XSSFWorkbook();
            XSSFSheet sheet=workbook.createSheet("result");
            XSSFRow row,row1;
            XSSFCell cell,cell1;
            
            row=sheet.createRow(1);
            cell=row.createCell(1);
                cell.setCellValue("Exam ID");
            cell=row.createCell(2);
                cell.setCellValue("Enrollment");
            cell=row.createCell(3);
                cell.setCellValue("Score");
            cell=row.createCell(4);
                cell.setCellValue("Correct");
            cell=row.createCell(5);
                cell.setCellValue("Wrong");
            cell=row.createCell(6);
                cell.setCellValue("Exam Name");
            cell=row.createCell(7);
                cell.setCellValue("Subject Code");
                
            int l=2;  
            while(rsE.next())
            {
                sTree.add(rsE.getString(2));
                
                row=sheet.createRow(l);
                cell=row.createCell(1);
                    cell.setCellValue(rsE.getString(2));
                cell=row.createCell(2);
                    cell.setCellValue(rsE.getString(3));
                cell=row.createCell(3);
                    cell.setCellValue(rsE.getInt(4));
                cell=row.createCell(4);
                    cell.setCellValue(rsE.getInt(5));
                cell=row.createCell(5);
                    cell.setCellValue(rsE.getInt(6));
                cell=row.createCell(6);
                    cell.setCellValue(rsE.getString(7));
                cell=row.createCell(7);
                    cell.setCellValue(rsE.getString(8));
                l++;    
            }
            
            //**********************Setup Info*********************************************************//
            Iterator it=sTree.iterator();
            while(it.hasNext())
            {
                rs1=st.executeQuery("Select * from quizsetup where setupid='"+it.next()+"'");
                while(rs1.next())
                {
                    eid.add(rs1.getString(8));
                    a.add(rs1.getString(3));
                    b.add(rs1.getString(4));
                    c.add(rs1.getString(5));
                    dd.add(rs1.getString(6));
                    e.add(rs1.getString(7));
                }
            }
            
            row1=sheet.createRow(l+5);
            
            cell1=row1.createCell(1);
                cell1.setCellValue("Exam ID");
            cell1=row1.createCell(2);
                cell1.setCellValue("Total Questions");
            cell1=row1.createCell(3);
                cell1.setCellValue("Mark for each");
            cell1=row1.createCell(4);
                cell1.setCellValue("Neative Marks");
            cell1=row1.createCell(5);
                cell1.setCellValue("Time");
            cell1=row1.createCell(6);
                cell1.setCellValue("Faculty");    
                
            int j=l+6;
            for(int k=0; k<eid.size();k++)
            {
                row=sheet.createRow(j);
                
                cell=row.createCell(1);
                    cell.setCellValue(eid.get(k));
                cell=row.createCell(2);
                    cell.setCellValue(a.get(k));
                cell=row.createCell(3);
                    cell.setCellValue(b.get(k));
                cell=row.createCell(4);
                    cell.setCellValue(c.get(k));
                cell=row.createCell(5);
                    cell.setCellValue(dd.get(k));
                cell=row.createCell(6);
                    cell.setCellValue(e.get(k));
                j++;
            }
            //**********************************Setup Info**********************************//
            
            try (FileOutputStream fos = new FileOutputStream(excelFile)) 
            {
                workbook.write(fos);
            }
            
            System.out.println("Report Generated");
            return excelFile;
        }
        catch(IOException | SQLException ex)
        {
            System.out.println(ex);
        }
        return null;
    }
}
