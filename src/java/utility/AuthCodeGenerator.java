/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import db.DBConnector;
import dto.FacultyDTO;
import java.sql.Statement;
import static utility.ApplicationConstants.EXP_MIN;
import static utility.ApplicationConstants.EMAIL_MESSAGE;
/**
 *
 * @author SHUBHAM
 */
public class AuthCodeGenerator 
{ 
    public boolean isCodeGeneated(FacultyDTO d)
    {
        String id=d.getFacultyid();
        String email=d.getEmail();
        
        String pw = OTPGeneration.otpGeneration();
        
        try
        {
            SendEmail sE = new SendEmail();
            sE.sendMail(email, "OTP For Examination", EMAIL_MESSAGE, "<h1>"+pw+"</h1>"
                    + "<p>OTP is valid for : </p><h3>"+EXP_MIN+" minutes only</h3>", null);
            Statement st=DBConnector.getStatement();
            int i=st.executeUpdate("update quizsetup set authcode='"+pw+"' where setupid='"+id+"'");
            if(i>0)
            {
            return true;
            }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        return false;
    }
  
}
