/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static utility.ApplicationConstants.REPORT_FILE;
/**
 *
 * @author SHUBHAM
 */
public class DownloadReport 
{
    public static void generateDownload(HttpServletRequest request, HttpServletResponse response,File my_file) throws IOException
    {
        response.setHeader("Content-disposition","attachment; filename="+REPORT_FILE); // Used to name the download file and its format

        OutputStream out = response.getOutputStream();
        FileInputStream in = new FileInputStream(my_file);
        byte[] buffer = new byte[4096];
        int length;
        while ((length = in.read(buffer)) > 0){
           out.write(buffer, 0, length);
        }
        in.close();
        out.flush();
        
    }
}
