/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

/**
 *
 * @author SHUBHAM
 */
// File Name SendEmail.java

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import java.io.File;

public class SendEmail 
{
    private final String from;       
    private final String password;
    private Properties props;
    private final Session session;
    private final MimeMessage message;
    private final Multipart multipart;

    public SendEmail() throws AddressException, MessagingException 
    {
        from = "capabilityshowcase@gmail.com"; 
        password = "Pepcus12345";
        props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        session = Session.getInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(from, password);
                }
        });
        message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
        multipart = new MimeMultipart();
    }

    public boolean sendMail(String to, String subject, String messageText, String htmlContent, String fileAttachment) 
        throws AddressException, MessagingException {
            return sendMail(to, subject, messageText, htmlContent, fileAttachment, null);
    }

    public boolean sendMail(String to, String subject, String messageText,String htmlContent, String fileAttachment, String fileName)
        throws AddressException, MessagingException {
        
            // Add passed Mail id to Recipient's Address.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set the Subject
            message.setSubject(subject);

            // create new multiple message parts
            MimeBodyPart messageBodyText = new MimeBodyPart();
            MimeBodyPart messageBodyHtml = new MimeBodyPart();
            MimeBodyPart messageAttachment = new MimeBodyPart();

            // Add the message text to first part
            messageBodyText.setText(messageText);
            // Add the message html to second part
            messageBodyHtml.setContent(htmlContent, "text/html");
            // Add Both parts to Multiple parts of final mail
            multipart.addBodyPart(messageBodyText);
            multipart.addBodyPart(messageBodyHtml);

            // Attach some file if given
            if (!(fileAttachment == null || fileAttachment.length() <= 0)) 
            {
                File file = new File(fileAttachment);
                // Source will be given absolute path
                DataSource source = new FileDataSource(fileAttachment);
                messageAttachment.setDataHandler(new DataHandler(source));

                // set default fileName if not given
                if (fileName == null || fileName.length() <= 0) 
                {
                    String tempFileAttachment = fileAttachment;
                    fileName = tempFileAttachment.substring((tempFileAttachment
                                    .replaceAll("\\\\", "/").lastIndexOf('/')) + 1,
                                        tempFileAttachment.length());
                }
                else 
                {
                    fileName = fileName + fileAttachment.substring(fileAttachment.lastIndexOf("."), fileAttachment.length());
                }

                // Custom file name that you want to give to your attachment
                // file
                messageAttachment.setFileName(fileName);
                // Add the message attachment to third part
                multipart.addBodyPart(messageAttachment);
            }

        // Add this part to Multiple parts of your of final mail
        message.setContent(multipart);
        // Send the message
        Transport.send(message);
        return true;
    }
}