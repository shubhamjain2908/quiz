/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

/**
 *
 * @author SHUBHAM
 */
public class ApplicationConstants 
{
    public final static String REPORT_FILE="report.xlsx";
    
    public static final int OTP_LENGTH=6;	
    public static final long EXP_MIN=5;		//OTP validity
    public static final String SECURE_GENERATION="SHA1PRNG";
    
    public static final String EMAIL_MESSAGE="Give the OTP to the students for taking quiz. "
            + "This OTP is valid for a limited amount of time only";
}
