/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.security.SecureRandom;
import java.util.stream.IntStream;
import static utility.ApplicationConstants.OTP_LENGTH;
import static utility.ApplicationConstants.SECURE_GENERATION;

/**
 *
 * @author SHUBHAM
 */
public class OTPGeneration 
{
    public static String otpGeneration() 
    {
        StringBuilder otp=new StringBuilder();
        try 
        {
            SecureRandom r=SecureRandom.getInstance(SECURE_GENERATION);	
            IntStream.range(0, OTP_LENGTH).forEach(i -> otp.append(r.nextInt(10)));
        }
        catch(Exception e) 
        {
            System.out.println(e);
        }
        return otp.toString();
    }
}
