<%-- 
    Document   : facultyHome
    Created on : Dec 1, 2017, 9:09:05 PM
    Author     : SHUBHAM
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="db.DBConnector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Faculty Home</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="assets/ico/qs-logo.png">
    </head>              
    <body>
        <style>
            table {
            overflow: scroll;
            }
        </style>
        <script src="editques.js"></script>
        <script src="checkBox.js"></script>
        <script>
            function login()
            {
                alert("Login success...");
            }
        </script>
        <%
            ResultSet rs=null,rs1=null,rs2=null,rs3=null;
            String fname="",email="",pno="",dob="",gen="";
            int flag=0;
            String facultyid=(String)session.getAttribute("facultyid");
            if(facultyid==null || facultyid.trim().equals(""))
            {
                response.sendRedirect("facultylogin.html");
            }
            try
            {
            Statement st=DBConnector.getStatement();
            String query="Select * from faculty where facultyid='"+facultyid+"'";
            System.out.println(query);
            rs=st.executeQuery(query);
            while(rs.next())
               {
                    fname=rs.getString(1);
                    email=rs.getString(3);
                    pno=rs.getString(4);
                    dob=rs.getString(5);
                    gen=rs.getString(6);
                    flag=rs.getInt(8);
               }
            }
            catch(SQLException e)
            {
                System.out.println(e);
            }
            %>    
            <script>
                    var request;
                    function sendSubjectInfo()
                    {
                        var v = document.subject.subjectcode.value;
                        var url = "findsubject.jsp?val=" + v;
                        if (window.XMLHttpRequest)
                        {
                            request = new XMLHttpRequest();
                        } else if (window.ActiveXObject)
                        {
                            request = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        try
                        {
                            request.onreadystatechange = getSubjectInfo;
                            request.open("GET", url, true);
                            request.send();
                        } catch (e)
                        {
                            alert("Unable to connect to server");
                        }
                    }
                    function getSubjectInfo()
                    {
                        if (request.readyState == 4)
                        {
                            var val = request.responseText;
                            document.getElementById('msg').innerHTML = val;
                        }
                    }
                    
            function setCheckbox(c) 
            {
                if (c.checked) 
		{
                    document.getElementById('isSelected').value="checked";
		}	 
		else 
		{
		    document.getElementById('isSelected').value="unchecked";
		}
            }        
            </script>        
        <%@include file="header.jsp"%>
        <style>
        .responsive {
            width: 100%;
            max-width: 650px;
            height: auto;
        }
    </style>
    <span id="confirm">
    	<%
            if (flag==0) {
            out.print("<font color='red' m>");
            out.print("<Marquee><h4>Not an Authorised Faculty!</h4></Marquee>");
            out.print("</font>");
            }
        %>	
    </span>
<div class="container well">
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
    <li><a data-toggle="tab" href="#profile">PROFILE</a></li>
    <li><a data-toggle="tab" href="#add_subject">Add Subject</a></li>
    <li><a data-toggle="tab" href="#quiz_setup">Quiz Setup</a></li>
    <li><a data-toggle="tab" href="#add_question">Add Question</a></li>
    <li><a data-toggle="tab" href="#update_question">Delete Question</a></li>
    <li><a data-toggle="tab" href="#edit_question">Edit Question</a></li>
    <li><a data-toggle="tab" href="#view_setup">View Setup</a></li>
    <li><a data-toggle="tab" href="#activeExam">Active Exam</a></li>
    <li><a data-toggle="tab" href="#view_result">View Result</a></li>
  </ul>
  <br>
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
     <div class="span10">
         <center><img src="images/center_box_bg.gif" alt="Online Exam System" class="responsive"/>  </center>
        <p style="font-weight: bold;font-size:20px;color:#808080;line-height: 25px;">
            <bold style="color:black;">Online Quiz</bold>
                <dl>
                        <dt>Features :</dt>
                            <dd>- Time Saver.</dd>
                            <dd>- Help the students to develop their skills.</dd>
                            <dd>- Randomized Question.</dd>
                            <dd>- Add different Subjects.</dd>
                            <dd>- Customized the Quiz.</dd>
                            <dd>- Edit or delete questions.</dd>
                            <dd>- View Result instantly.</dd>
                            <dd>- Generate Result Report.</dd>
                </dl>
        </p><br><br><br>
     </div>
    </div>
    <div id="profile" class="tab-pane fade">
        <div class="container">       
  <div class="col-md-4 col-md-offset-4">
      <h1>Personal Details :</h1>
  <table style="overflow-x:auto;" class="table table-responsive table-bordered table-hover"> 
    <tbody>
      <tr>
          <td><strong>Name :</strong></td>
        <td><%=fname%></td>
      </tr>
      <tr>
        <td><strong>ID :</strong></td>
        <td><%=facultyid%></td>
      </tr>
      <tr>
        <td><strong>Email :</strong></td>
        <td><%=email%></td>
      </tr>
      <tr>
        <td><strong>Phone Number :</strong></td>
        <td><%=pno%></td>
      </tr>
      <tr>
        <td><strong>DOB :</strong></td>
        <td><%=dob%></td>
      </tr>
      <tr>
        <td><strong>Gender :</strong></td>
        <td><%=gen%></td>
      </tr>
    </tbody>
  </table>
      <a href="#" data-toggle="modal" data-target="#changePasswordModal">Change Password</a><br><br><br>
          <div id="changePasswordModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Change Password</h4>
      </div>
        
      <div class="modal-body">
            <form action="FacultyPassChange" method="post" onsubmit="return confirm('Are you sure ?');"> 
   <table style="overflow-x:auto;" class="table table-responsive table-striped table-hover"> 
    <tbody>
      <tr>
        <td><strong>Faculty ID :</strong></td>
        <td><input type="text" name="facultyid" value="<%=facultyid%>" readonly /></td>
      </tr>
      <tr>
        <td><strong>Current Password :</strong></td>
        <td><input type="password" name="password" placeholder="******" required/></td>
      </tr>
      <tr>
        <td><strong>New Password :</strong></td>
        <td><input type="password" name="newpassword" placeholder="******" required /></td>
      </tr>
      <tr>
        <td><button type="reset" class="btn btn-primary " data-loading-text="Loading...">Reset</button></td>
        <td><button type="submit" class="btn btn-primary btn-success" data-loading-text="Loading...">Submit</button></td>
      </tr>
    </tbody>
  </table>
      </form>
      </div>
        
      <div class="modal-footer">
          <button type="button" class="btn btn-default btn-block" data-dismiss="modal"><span class="glyphicon glyphicon-cloud"></span> Close</button>
      </div>
    </div>
  </div>
</div>
  </div>
</div>
    </div>
      <!------------------------------------------------>
        <%
            if(flag!=0)
            {
        %>
    <div id="add_subject" class="tab-pane fade">
     <div class="container">       
     <div class="col-md-4 col-md-offset-4">
         <strong><h1>Subject Registration :</h1></strong>
         <form action="SubjectChecker" method="post" name="subject" onsubmit="return confirm('Are you sure ?');">
               <table style="overflow-x:auto;" class="table table-responsive table-bordered table-hover">
                <tr>
                    <td><strong>Subject Code:</strong></td>
                    <td><input type="text" name="subjectcode" required onkeyup="sendSubjectInfo()" /></td>
                </tr>
                <tr>
                    <td><strong>Subject Name:</strong></td>
                    <td><input type="text" required name="subjectname"/></td>
                </tr>
                <tr>
                    <td><button type="reset" class="btn btn-primary " >Reset</button></td>
                    <td><button type="submit" class="btn btn-primary btn-success">Submit</button></td>
                </tr>
            </table>
        </form>
         <br>
  <a href="#" data-toggle="modal" data-target="#viewSubjectModal">View Subjects</a><br><br><br>
  <div id="viewSubjectModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit</h4>
      </div>
        
      <div class="modal-body">
          <table style="overflow-x:auto;" class="table table-responsive table-striped table-hover">
              <tbody>
                  <%
            try
            {
            Statement st1=DBConnector.getStatement();
            String query2="Select * from addsubject";
            rs2=st1.executeQuery(query2);
        %>
      <tr>
        <td><strong>Subject Code :</strong></td>
        <td><strong>Subject Name :</strong></td>
      </tr>
        <%
             while(rs2.next())
               {
        %>
        <tr> 
          <td><%= rs2.getString(1)%></td>
          <td><%= rs2.getString(2)%></td>
        </tr>
        <%      }
             }
             catch(SQLException e)
             {
                System.out.println(e);
             } 
        %>
              </tbody>
          </table>
      </div>
        
      <div class="modal-footer">
          <button type="button" class="btn btn-default btn-block" data-dismiss="modal"><span class="glyphicon glyphicon-cloud"></span> Close</button>
      </div>
        
    </div>

  </div>
</div>
         <div id="msg" style="font-style: inherit; font-size: large; color: red;"><strong></strong></div>
    </div>
   </div>
    </div>
      
    <div id="quiz_setup" class="tab-pane fade">
   <div class="container">       
     <div class="col-md-5 col-md-offset-3">
         <strong><h1>Exam Details :</h1></strong>
           <form action="ExamSetupChecker" method="post" onsubmit="return confirm('Are you sure ?');">
               <table style="overflow-x:auto;" class="table  table-responsive table-bordered table-hover">
                <tr>
                    <td><strong>Faculty Name:</strong></td>
                    <td><input class="form-control" type="text" value="<%=fname%>" readonly name="fname" /></td>
                </tr>
                <tr>
                    <td><strong>Subject Name:</strong></td>
                    <td><select class="form-control" name="subject" required>
                    <%
                        try{
                        Statement st=DBConnector.getStatement();
                        rs3=st.executeQuery("Select * from addsubject");
                    %>
                    <option value="">--Select Subject--</option>
                    <%
                        while(rs3.next())
                    {
                    %>
                    <option value="<%=rs3.getString(1)%>"><%=rs3.getString(1)%> : <%=rs3.getString(2)%></option>
                     <%      }
                    }
                        catch(SQLException e)
                        {
                        System.out.println(e);
                        }
        %>
                </select></td>
                </tr>
                <tr>
                    <td><strong>Exam Name:</strong></td>
                    <td><input class="form-control" type="text" required name="examname" /></td>
                </tr>
                <tr>
                    <td><strong>Question Level:</strong></td>
                    <td><select class="form-control" name="level" required>
                            <option value="">--Select Question Level--</option>
                            <option value="Easy">Easy</option>
                            <option value="Moderate">Moderate</option>
                            <option value="Difficult">Difficult</option>
                            <option value="Random">Random</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><strong>Total Questions:</strong></td>
                    <td><input class="form-control" type="number" required min="1"  name="totalquestion"/></td>
                </tr>
                <tr>
                    <td><strong>Marks for Each:</strong></td>
                    <td><input class="form-control" type="number" required min="1"  name="marksforeach"/></td>
                </tr>
                <tr>
                    <td><strong>Negative Marks:</strong></td>
                    <td><input class="form-control" type="number" required min="0" name="negativemarks"/></td>
                </tr>
                <tr>
                    <td><strong>Time Duration:</strong></td>
                    <td><input class="form-control" type="number" required min="0" step="5" name="timeduration"/></td>
                </tr>
                <tr>
                    <td><button type="reset" class="btn btn-primary " data-loading-text="Loading...">Reset</button></td>
                    <td><button type="submit" class="btn btn-primary btn-success" data-loading-text="Loading...">Submit</button></td>
                </tr>
            </table><br><br><br>
        </form>
    </div>
   </div>
 </div>
        
    <div id="add_question" class="tab-pane fade">
        <div class="row">
        <div class="col-md-8 col-md-offset-2">
         <strong><h1>Insert Questions :</h1></strong>
        <form role="form" action="QuestionChecker" method="post">
        <div class="form-group">
            <div class="selectContainer">
                <select class="form-control" name="subject" required>
                    <%
                        try{
                        Statement st=DBConnector.getStatement();
                        rs3=st.executeQuery("Select * from addsubject");
                    %>
                    <option value="">--Select Subject--</option>
                    <%
                        while(rs3.next())
                    {
                    %>
                    <option value="<%=rs3.getString(1)%>"><%=rs3.getString(1)%> : <%=rs3.getString(2)%></option>
                     <%      }
                }
            catch(SQLException e)
            {
                System.out.println(e);
            }
        %>
                </select><br>
                <select class="form-control" name="level" required>
                    <option value="">--Select Question Level--</option>
                    <option value="Easy">Easy</option>
                    <option value="Moderate">Moderate</option>
                    <option value="Difficult">Difficult</option>
                </select>
            </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="question">Question</label>
            <div class="controls">
        <textarea class="form-control span5" name="question" id="question" rows="5" placeholder="Write any question......" required="true"></textarea>
        <!--<span id="err"> </span>-->
            </div>
	</div>
        <table style="overflow-x:auto;" class="table table-responsive table-hover">
                <tr>
                    <td><strong><label><input type="radio" value="A" name="ans" required>A</label></strong></td>
                    <td><textarea class="form-control" name="optionA" class="col-md-12" id="optionA" placeholder="Option A" required="true"></textarea></td>
                </tr>
                <tr>
                    <td><strong><label><input type="radio" value="B" name="ans" required>B</label></strong></td>
                    <td><textarea class="form-control" name="optionB" class="col-md-12" id="optionB" placeholder="Option B" required="true"></textarea></td>
                </tr>
                <tr>
                    <td><strong><label><input type="radio" value="C" name="ans" required>C</label></strong></td>
                    <td><textarea class="form-control" name="optionC" class="col-md-12" id="optionC" placeholder="Option C" required="true"></textarea></td>
                </tr>
                <tr>
                    <td><strong><label><input type="radio" value="D" name="ans" required>D</label></strong></td>
                    <td><textarea class="form-control" name="optionD" class="col-md-12" id="optionD" placeholder="Option D" required="true"></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td><button type="reset" class="btn btn-primary " >Reset</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary btn-success">Submit</button></td>
                </tr>
            </table><br><br><br>
      </form>
            </div>
        </div>
    </div>
                
    <div id="update_question" class="tab-pane fade">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
         <strong><h1>Delete Questions :</h1></strong>
         <form name="editQ">
        <div class="form-group">
            <div class="selectContainer"><input hidden name="d" value="delete"/>
                <select class="form-control" name="subject" onchange="sendQuesInfo()" required>
                    <%
                        try{
                        Statement st=DBConnector.getStatement();
                        rs3=st.executeQuery("Select * from addsubject");
                    %>
                    <option value="">--Select Subject--</option>
                    <%
                        while(rs3.next())
                    {
                    %>
                    <option value="<%=rs3.getString(1)%>"><%=rs3.getString(1)%> : <%=rs3.getString(2)%></option>
                    <%      }
                }
            catch(SQLException e)
            {
                System.out.println(e);
            }
        %>
                </select>
            </div>
        </div>
        </form>
        </div>
      </div>
                <div id="editquestion">
                    
                </div><br><br><br>
    </div>
    
    <div id="edit_question" class="tab-pane fade">
        <form name="editQu" method="get" action="#">
            <input hidden name="e" value="edit"/>
            <input type="search" class="form-control" name="eques" onkeyup="editQues()" placeholder="enter question id..."/>
        </form>
        <div id="edQ">
            
        </div><br><br><br>
    </div>  
                
    <div id="activeExam" class="tab-pane fade">
        <div class="col-md-6 col-md-offset-3">
        <form name="activeE" method="post" action="AuthCode">
            <input hidden type="text" name="email" value="<%=email%>" >
            <input type="search" class="form-control" required name="acId" placeholder="enter setup id..."/>
            <div style="padding-top: 30px;">
                <button type="submit" class="btn btn-warning" style="width:220px;height:50px;margin:auto;display:block">Generate Authentication Code!</button>
            </div>
        </form>
        </div>    
    </div>
                
    <div id="view_setup" class="tab-pane fade">
      <%
            try
            {
            Statement st1=DBConnector.getStatement();
            String query2="Select * from quizsetup where faculty='"+fname+"'";
            rs2=st1.executeQuery(query2);
      %>
<form action="DeleteExamS" method="post" onsubmit="return confirm('Are you sure ?');">  
    <input hidden type="text" id="isSelected" name="isSelected" value="unchecked"/>
  <table style="overflow-x:auto;" class="table table-responsive table-bordered table-hover"> 
    <tbody>
      <tr>
        <td><strong>ID :</strong></td>  
        <td><strong>Subject Code :</strong></td>
        <td><strong>Exam Name :</strong></td>
        <td><strong>Total Question :</strong></td>
        <td><strong>Question Level:</strong></td>
        <td><strong>Marks for Each :</strong></td>
        <td><strong>Negative Marks :</strong></td>
        <td><strong>Time Duration :</strong></td>
<!--        <td><strong>OTP :</strong></td>-->
        <td><strong>Delete :</strong>&nbsp;&nbsp;&nbsp;<input type="checkbox" id="selectall"/></td>
      </tr>
        <%
             while(rs2.next())
               { 
        %>          
        <tr> 
          <td><%= rs2.getString(8)%></td>  
          <td><%= rs2.getString(1)%></td>
          <td><%= rs2.getString(2)%></td>
          <td><%= rs2.getString(3)%></td>
          <td><%= rs2.getString(10)%></td>
          <td><%= rs2.getString(4)%></td>
          <td><%= rs2.getString(5)%></td>
          <td><%= rs2.getString(6)%></td>
<!--          <td><%= rs2.getString(9)%></td>-->
          <td><input type="checkbox" class="delete" name="delete" id="delete" onchange="setCheckbox(this)" value=<%=rs2.getString(8)%>></td>
        </tr>
        <%      }
             }
             catch(SQLException e)
             {
                System.out.println(e);
             } 
        %>
        <tr> 
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
<!--          <td></td>-->
          <td></td>
          <td><button type="submit" name="submit" value="off" class="btn btn-danger "><span class="glyphicon glyphicon-off"></span> Deactive</button></td>
          <td><button type="submit" name="submit" value="del" class="btn btn-danger "><span class="glyphicon glyphicon-trash"></span> Delete</button></td>
        </tr>
    </tbody>
  </table>
</form>
    </div>
    <div id="view_result" class="tab-pane fade">
        <table style="overflow-x:auto;" class="table table-responsive table-bordered table-hover">
            <tbody>
                <tr>
                  <td><strong>Subject Code :</strong></td>  
                  <td><strong>Exam Name :</strong></td>  
                  <td><strong>Enrollment :</strong></td>
                  <td><strong>Score :</strong></td>
                  <td><strong>Correct Attempts :</strong></td>
                  <td><strong>Wrong Attempts :</strong></td>
                </tr>
        <%
            ArrayList<Integer> sid=new ArrayList<Integer>();
            try
            {
                Statement stR=DBConnector.getStatement();
                ResultSet rsR=null,rsR1=null;
                rsR=stR.executeQuery("Select setupid from quizsetup where faculty='"+fname+"'");
                while(rsR.next())
                {
                    sid.add(rsR.getInt("setupid"));
                }
                for (int i = 0; i < sid.size(); i++) 
                {
                        rsR1=stR.executeQuery("Select * from result where Examid='"+sid.get(i)+"'");
                        while(rsR1.next())
                        {
                            %>
                <tr>
                  <td><%=rsR1.getString(8)%></td>  
                  <td><%=rsR1.getString(7)%></td>  
                  <td><%=rsR1.getString(3)%></td>
                  <td><%=rsR1.getString(4)%></td>
                  <td><%=rsR1.getString(5)%></td>
                  <td><%=rsR1.getString(6)%></td>
                </tr>                
                            <%
                        }
                }
            }
            catch(Exception e)
            {
                System.out.println(e);
            }
        %>
            </tbody>
        </table>
            <br>
            <form action="Report" method="post">
                <input hidden type="text" value="<%=facultyid%>" name="user">
                <input hidden type="text" value="complete" name="about">
                <div style="padding-top: 30px;">
        <button type="submit" class="btn btn-warning" style="width:200px;height:50px;margin:auto;display:block">Generate Result Report!</button>
                </div><br><br><br>
            </form>
    </div>
            <%}%>
            <!------------------------------------------------>
  </div>
</div>
        <!--Footer-->
        <%@include file="footer.jsp"%>
    </body>
</html>
