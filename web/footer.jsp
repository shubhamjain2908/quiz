<%-- 
    Document   : footer
    Created on : Dec 2, 2017, 5:24:00 PM
    Author     : SHUBHAM
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="footer">       
                <div class="navbar navbar-fixed-bottom">
                    <div class="navbar-inner" style="background:#000;border-bottom:0px;">                                         
<center style="color:white"><a href="#contact"  data-toggle="modal" >Developer</a> || <a href="#feed" data-toggle="modal">Feedback</a></center>
<center style="color: white">Copyright &copy; 2018 Quiz Show, All rights reserved Disclaimer</center>
<center>The website is at its BETA phase. Please help us to improve the website by giving your feedback.</center>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="contact" role="dialog">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Contact.</h4>
                                    </div>
                                      <centre>
                                    <div align="center" style='color:#808080; font-weight: bold;' class="modal-body">
                                      <ul class="list-group">
                                          <li class="list-group-item list-group-item-success">Shubham Jain</li>
                                        <li class="list-group-item list-group-item-info"><strong>Mobile : </strong>+91 8871574375</li>
                                        <li class="list-group-item list-group-item-danger"><strong>Email : </strong>Shubhamjain2908@gmail.com</li>
                                        <li class="list-group-item list-group-item-warning"><strong>Branch : </strong>Information Technology</li>
                                        <li class="list-group-item list-group-item-danger">Medicaps Institute of Technology & Management </li>
                                        <li class="list-group-item list-group-item-info"><strong>Session : </strong>2015-2019</li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                            </div>
            <div class="modal fade" id="feed">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="Feedback" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                    <h3 id="myModalLabel">Feedback Form</h3>
                            </div>
                    <div align="center" class="modal-body lead" style='color:#808080 ;font-weight: bold;' >
                        	<label class="control-label" for="comment">Comment</label>
                        		<div class="controls">
            <textarea class="form-control" name="comment" id="comment" rows="8" placeholder="Put your Comment......" required="true"></textarea>
                                </div>
                                <span id="confirm">
                                	<%
                                        if (request.getAttribute("Error") != null) {
                                            out.print("<font size='2' color='red' m>");
                                            out.print(request.getAttribute("Error"));
                                            out.print("</font>");
                                        }
                                    %>	
                                </span>
                    </div>
                	<div class="control-group">
                    	<label class="control-label"></label>
                        	<div class="controls">
                            	<button type="submit" class="btn btn-primary btn-block" data-loading-text="Loading..."><span class="glyphicon glyphicon-comment"></span> Submit</button>
                        	</div>
                	</div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>