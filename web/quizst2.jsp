<%-- 
    Document   : quizst2
    Created on : Dec 18, 2017, 1:53:40 AM
    Author     : SHUBHAM
--%>

<%@page import="java.util.Collections"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="db.DBConnector"%>
<html>
<head>
  <title>Quiszz</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-animate.js"></script>
  <link rel="shortcut icon" href="assets/ico/favicon.png">
</head>
<%@include file="header.jsp" %>
<body background="images/97f033911247bfa82b4f7cb0ae0fed7d5fd17768_hq.gif" onload="hide()">
    <script>
        window.onbeforeunload = function() 
        {
            return "Your work will be lost.";
        };
    </script>
    
<% 
    String enrollment=(String)session.getAttribute("enrollment");
    ArrayList<Integer> qid1=new ArrayList<>();
    String eid=(String)session.getAttribute("examname");  //Setup id
    String scode=(String)session.getAttribute("scode"); //Subject Code
    int time=(int)session.getAttribute("time");
    int totalquestion=(int)session.getAttribute("totalquestion");
    int marks=(int)session.getAttribute("marks");
    int negative=(int)session.getAttribute("negative");
    String enam=(String)session.getAttribute("ename");    //Setup Name
    qid1=(ArrayList<Integer>)session.getAttribute("question");
    Collections.shuffle(qid1);
    try
    {
        Statement st=DBConnector.getStatement();
%>
         <table hidden class="table table-responsive table-bordered table-hover"  id="t"> 
          <tr class="header">
            <td><strong>Question:</strong></td>
            <td><strong>Option A :</strong></td>
            <td><strong>Option B :</strong></td>
            <td><strong>Option C :</strong></td>
            <td><strong>Option D :</strong></td>
            <td><strong>Answer :</strong></td>
            <td><strong>Answer selected :</strong></td>
          </tr>
        <%
        for(int l=0;l<qid1.size();l++)
        {
        String query="Select * from questions where qid='"+qid1.get(l)+"'";  //
        ResultSet rs=st.executeQuery(query);
            while(rs.next())
            {
                %>
        <tr> 
          <td><%= rs.getString(3)%></td>
          <td><%= rs.getString(4)%></td>
          <td><%= rs.getString(5)%></td>
          <td><%= rs.getString(6)%></td>
          <td><%= rs.getString(7)%></td>
          <td><%= rs.getString(8)%></td>
          <td></td>
        </tr>
        <%
            }
        }
        %>
        </table>
        <%
    }
    catch(Exception e)
    {
        System.out.println(e);
    }
        %>
        <div id="hideA" class="container container-fluid" style="padding-top: 100px;margin:auto;">
            <h1 style="font-style: italic; color: red;"><centre>Read Instructions First...</centre></h1>
            <br>
            <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" style="width:250px;height:60px;margin:auto;display:block" data-toggle="modal" data-target="#myModalL">Instruction</button>
  <!-- Modal -->
  <div class="modal fade" id="myModalL" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Read instruction carefully.</h4>
        </div>
          <centre>
        <div align="center" style='color:#808080; font-weight: bold;' class="modal-body">
          <ul class="list-group">
            <li class="list-group-item list-group-item-success">1. You have <%=time%> minutes.</li>
            <li class="list-group-item list-group-item-info">2 . There are <%=totalquestion%> questions.</li>
            <li class="list-group-item list-group-item-danger">3 . Every question is mandatory.</li>
            <li class="list-group-item list-group-item-warning">4 . For every correct attempt you will get <%=marks%> marks.</li>
            <li class="list-group-item list-group-item-danger">5 . <%=negative%> marks will be deducted in any other case.</li>
            <li class="list-group-item list-group-item-info">6 . All questions have four options out of which only one is correct.</li>
          </ul>
        </div>        
        <div class="modal-footer">
            <button type="button" class="btn btn-success btn-block" onclick="unhide()" data-dismiss="modal">Start Quiz..!!!</button>
        </div>
      </div>
    </div>
  </div>
        </div>
  <div id="hideB" class="container container-fluid">  
      <div class="row">
           <div class="col-md-2" style="padding-top: 150px;">
<!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Instruction</button>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Read instruction carefully.</h4>
        </div>
          
        <div align="center" style='color:#808080; font-weight: bold;' class="modal-body">
          <ul class="list-group">
            <li class="list-group-item list-group-item-success">1. You have <%=time%> minutes.</li>
            <li class="list-group-item list-group-item-info">2 . There are <%=totalquestion%> questions.</li>
            <li class="list-group-item list-group-item-danger">3 . Every question is mandatory.</li>
            <li class="list-group-item list-group-item-warning">4 . For every correct attempt you will get <%=marks%> marks.</li>
            <li class="list-group-item list-group-item-danger">5 . <%=negative%> marks will be deducted in any other case.</li>
            <li class="list-group-item list-group-item-info">6 . All questions have four options out of which only one is correct.</li>
          </ul>
        </div>        
      </div>
    </div>
  </div>
 </div>
          
                <div class="col-md-8">
            <strong><h1>Questions :</h1></strong>  
            <form role="form" action="#" method="post" name="test">
        <div class="control-group">
            <label id="questionnumber" class="control-label" for="question">1</label>
            <input type="text" name="questionnumber" hidden value="1"> 
            <div class="controls">
                <textarea class="form-control span5" name="question" id="question" rows="5" placeholder="Write any question......" readonly required="true"></textarea>
        <!--<span id="err"> </span>-->
            </div>
	</div>
        <table class="table table-responsive">
                <tr>
                    <td><strong><label><input type="radio" value="A" id="oA" name="ans" required>A</label></strong></td>
                    <td><textarea name="optionA" class="col-md-12" id="optionA" placeholder="Option A" readonly required="true"></textarea></td>
                </tr>
                <tr>
                    <td><strong><label><input type="radio" value="B" id="oB" name="ans" required>B</label></strong></td>
                    <td><textarea name="optionB" class="col-md-12" id="optionB" placeholder="Option B" readonly required="true"></textarea></td>
                </tr>
                <tr>
                    <td><strong><label><input type="radio" value="C" id="oC" name="ans" required>C</label></strong></td>
                    <td><textarea name="optionC" class="col-md-12" id="optionC" placeholder="Option C" readonly required="true"></textarea></td>
                </tr>
                <tr>
                    <td><strong><label><input type="radio" value="D" id="oD" name="ans" required>D</label></strong></td>
                    <td><textarea name="optionD" class="col-md-12" id="optionD" placeholder="Option D" readonly required="true"></textarea></td>
                </tr>
                <tr>
                    <td><input type="button" class="btn bg-warning " value="Clear" onclick="clearAns(ans);" ></td>
                    <td><input type="button" class="btn btn-primary " value="Previous" onclick="previous()" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="btn btn-primary btn-success" value="Next" onclick="next()"></td>
                </tr>
            </table>
      </form>
                
      <form name="sub" id="sub" onsubmit="return confirm('Are you sure ?');" action="Result" method="post">
          <input type="text" name="score" id="score" value="" hidden />
          <input type="text" name="correct" id="correct" value="" hidden />
          <input type="text" name="wrong" id="wrong" value="" hidden />
          <input type="text" name="eid" id="eid" value="<%=eid%>" hidden />
          <button type="submit" class="btn btn-danger" style="width:140px;height:50px;margin:auto;display:block" onclick="marks()"><span class="glyphicon glyphicon-stop"></span> Submit Quiz..!!!</button>
      </form>
          </div>  
          <div class="col-md-2" id="testA" style="display:block; ">
              
              <br>
              <h3><span class="glyphicon glyphicon-time"></span> <kbd id="tim"></kbd></h3>
              <br>
              <table class="table table-responsive table-sm table-bordered table-hover"  id="stuC">
                  <tbody>
                      <tr>
                          <td>Question Number</td>
                          <td>Answer Selected</td>
                      </tr>
                      <%for (int i = 0; i <qid1.size(); i++) {
                       %>
                      <tr>
                          <td><%=i+1%></td>
                          <td></td>
                      </tr>
                      <%       
                          }
                      %>
                  </tbody>
              </table>
          </div>
</div>
</div>
    <script type="text/javascript">
        var v=1,total=parseInt("<%=totalquestion%>");
        var ta=document.getElementById('t');
        var tc=document.getElementById('stuC');
        var a=null;
        var score=0,correct=0,wrong=0;
        var mrks=parseInt("<%=marks%>");
        var nmrks=parseInt("<%=negative%>");
        var timeAlloted=parseInt("<%=time%>")*60000;
        
        function hide()
        {
            document.getElementById("hideB").style.visibility="hidden";
            //alert("Read Instruction First...!!!");
            //alert(timeAlloted);
        }
        function unhide()
        {
            document.getElementById("hideB").style.visibility="visible";
            document.getElementById("hideA").style.display="none";
            first(); 
            time_spent();
            timer();
        }
    /**************************************************************************/
    /****************************Time Elapsed**********************************/
    var startDate = new Date();
    var startTime = startDate.getTime();

    function seconds_elapsed () 
    { 
        var date_now = new Date (); 
        var time_now = date_now.getTime (); 
        var time_diff = time_now - startTime; 
        var seconds_elapsed = Math.floor ( time_diff / 1000 ); 
        return ( seconds_elapsed ); 
    } 

    function time_spent () 
    { 
        var secs = seconds_elapsed ();
        var mins = Math.floor ( secs / 60 );
        secs -= mins * 60;
        var hour = Math.floor ( mins / 60 );
        mins -= hour * 60;
        document.getElementById('tim').innerHTML=pad ( hour ) + ":" + pad ( mins ) + ":" + pad ( secs );
        setTimeout( "time_spent ()", 1000 ); 
    }
    
    function pad ( num )
    {
        return ( ( num > 9 ) ? num : "0" + num );
    }
    /**************************Time Elapsed************************************/
    /**************************************************************************/
    function selectedAns()
    {
        var radios = document.getElementsByName('ans');
        for (var j = 0, length = radios.length; j < length; j++)
        {
            if (radios[j].checked)
            {
                // do whatever you want with the checked radio
                a = radios[j].value; 
                clearAns(document.test.ans);
                // only one radio can be logically checked, don't check the rest
                break;
            }
            else{
                a="";
            }
        }
    }
    function selectedAnsLast()
    {
        var radios = document.getElementsByName('ans');
        for (var j = 0, length = radios.length; j < length; j++)
        {
            if (radios[j].checked)
            {
                // do whatever you want with the checked radio
                a = radios[j].value; 
                //clearAns(document.test.ans);
                // only one radio can be logically checked, don't check the rest
                break;
            }
            else{
                a="";
            }
        }
    }
    function clearAns(buttonGroup)
    {
      for (i=0; i < buttonGroup.length; i++) {
        if (buttonGroup[i].checked == true) { // if a button in group is checked,
              buttonGroup[i].checked = false;  // uncheck it
          }
      }  
    }
    function marks()
    {
        for (var z = 0; z < total; z++) 
        {
            if(ta.rows[z+1].cells[5].innerHTML == ta.rows[z+1].cells[6].innerHTML)
            {
                score = score+mrks;
                correct++;
            }
            else
            {
                score = score-nmrks;
                wrong++;
            }
        }
        document.getElementById('score').value=score;
        document.getElementById('correct').value=correct;
        document.getElementById('wrong').value=wrong;
        //alert(score);
    }
    function first()
    {
        //document.getElementById('questionnumber').innerHTML =++v;
        document.getElementById('question').innerHTML=ta.rows[1].cells[0].innerHTML;
        document.getElementById('optionA').innerHTML=ta.rows[1].cells[1].innerHTML;
        document.getElementById('optionB').innerHTML=ta.rows[1].cells[2].innerHTML;
        document.getElementById('optionC').innerHTML=ta.rows[1].cells[3].innerHTML;
        document.getElementById('optionD').innerHTML=ta.rows[1].cells[4].innerHTML;
    }
    function next() 
    {
        if (v<total) 	
        {
            document.getElementById('questionnumber').innerHTML =++v;
            document.getElementById('question').innerHTML=ta.rows[v].cells[0].innerHTML;
            document.getElementById('optionA').innerHTML=ta.rows[v].cells[1].innerHTML;
            document.getElementById('optionB').innerHTML=ta.rows[v].cells[2].innerHTML;
            document.getElementById('optionC').innerHTML=ta.rows[v].cells[3].innerHTML;
            document.getElementById('optionD').innerHTML=ta.rows[v].cells[4].innerHTML;

            selectedAns();
            ta.rows[v-1].cells[6].innerHTML=a;
            tc.rows[v-1].cells[1].innerHTML=a;
            clearAns(document.test.ans);
            var s=ta.rows[v].cells[6].innerHTML;
            if(s != null)
            {
                if(s == 'A')
                {
                    document.getElementById('oA').checked = true;
                }
                if(s == 'B')
                {
                    document.getElementById('oB').checked = true;
                }
                if(s == 'C')
                {
                    document.getElementById('oC').checked = true;
                }
                if(s == 'D')
                {
                    document.getElementById('oD').checked = true;
                }
                //else
                //{
                  //  alert("None selected");
                //}
            }
        }
        else
        {
            alert("This is the last Question!!!");
            selectedAnsLast();
            ta.rows[v].cells[6].innerHTML=a;
            tc.rows[v].cells[1].innerHTML=a;
        }
    }

    function previous() 
    {
        if(v>1)
        {
            document.getElementById('questionnumber').innerHTML =--v;
            document.getElementById('question').innerHTML=ta.rows[v].cells[0].innerHTML;
            document.getElementById('optionA').innerHTML=ta.rows[v].cells[1].innerHTML;
            document.getElementById('optionB').innerHTML=ta.rows[v].cells[2].innerHTML;
            document.getElementById('optionC').innerHTML=ta.rows[v].cells[3].innerHTML;
            document.getElementById('optionD').innerHTML=ta.rows[v].cells[4].innerHTML;

            selectedAns();
            ta.rows[v+1].cells[6].innerHTML=a;
            tc.rows[v+1].cells[1].innerHTML=a;
            clearAns(document.test.ans);
            var s=ta.rows[v].cells[6].innerHTML;
            if(s != null)
            {
                if(s == 'A')
                {
                    document.getElementById('oA').checked = true;
                }
                if(s == 'B')
                {
                    document.getElementById('oB').checked = true;
                }
                if(s == 'C')
                {
                    document.getElementById('oC').checked = true;
                }
                if(s == 'D')
                {
                    document.getElementById('oD').checked = true;
                }
                //else
                //{
                  //  alert("None selected");
                //}
            }
        }
        else
        {
            alert("This is the First Question!!!");
        }
    }
    function time()
    {
        marks();
        document.getElementById("sub").submit();
    }
    function timer()
    {
        setTimeout(function(){ time();}, timeAlloted);
    }
    </script>        
</body>
<%@include file="footer1.jsp" %>
</html>