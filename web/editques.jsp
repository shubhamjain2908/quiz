<%-- 
    Document   : editques
    Created on : Dec 17, 2017, 6:14:03 PM
    Author     : SHUBHAM
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="db.DBConnector"%>
<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%
    String about=request.getParameter("about");
        String n=request.getParameter("val");
        
        System.out.println(about);
        //about="delete";
        System.out.println(about + n);
        if(about.equals("delete"))
        {
        //System.out.println(n);
        if(n.length()>0){
            try
            {
                Statement st=DBConnector.getStatement();
                String qu="Select * from questions where Subject_code='"+n+"'";
                ResultSet rs=st.executeQuery(qu);
                %>
        <form action="DeleteQuestion" onsubmit="return confirm('Are You sure ?');" method="post">   
        <table class="table table-responsive table-bordered table-hover"> 
        <tbody>
          <tr>
            <td><strong>Question ID</strong></td>
            <td><strong>Question :</strong></td>
            <td><strong>Option A :</strong></td>
            <td><strong>Option B :</strong></td>
            <td><strong>Option C :</strong></td>
            <td><strong>Option D :</strong></td>
            <td><strong>Answer :</strong></td>
            <td><strong>Level :</strong></td>
            <td><strong>Delete:</strong></td>
          </tr>
                <%
                while(rs.next())
                {
                %>
        <tr>
            <td><%=rs.getInt(1)%></td>
            <td><%=rs.getString(3)%></td>
            <td><%=rs.getString(4)%></td>
            <td><%=rs.getString(5)%></td>
            <td><%=rs.getString(6)%></td>
            <td><%=rs.getString(7)%></td>
            <td><%=rs.getString(8)%></td>
            <td><%=rs.getString(10)%></td>
            <td><input type="checkbox" name="delete" value=<%=rs.getInt(1)%>></td>
        </tr>
                <%
                }
            }
            catch(Exception e)
            {
                System.out.println(e);
            }
                 %>
        <tr> 
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td><button type="submit" name="submit" class="btn btn-danger ">Delete</button></td>
        </tr>
    </tbody>
  </table>
</form>
        <%
    }
}

    if(about.equals("edit"))
    {
        try
        {
            Statement st=DBConnector.getStatement();
            ResultSet rs1=st.executeQuery("Select * from questions where qid='"+n+"'");
            while(rs1.next())
            {
%><br>
<form action="QuestionChecker" method="post" onsubmit="return confirm('Are you sure ?');">
    <div class="col-md-8 col-md-offset-2"><input type="text" hidden name="qid" value="<%=n%>">
    <div class="control-group">
        <label class="control-label" for="question"><Strong>Correct Answer : </Strong></label>&nbsp;&nbsp;&nbsp;<span><label class="control-label" for="corr"><%=rs1.getString(8)%></label></span><br>
        <label class="control-label" for="level"><Strong>Question Level : </Strong></label>&nbsp;&nbsp;&nbsp;<span><label class="control-label" for="level"><%=rs1.getString(10)%></label></span>    
        <div class="controls">
        <textarea class="form-control span5" name="question" id="question" rows="5" placeholder="Write any question......" required="true"><%=rs1.getString(3)%></textarea>
        <!--<span id="err"> </span>-->
            </div>
	</div>
        <table class="table table-responsive table-hover">
                <tr>
                    <td><strong><label><input type="radio" value="A" name="ans" required>A</label></strong></td>
                    <td><textarea name="optionA" class="col-md-12" id="optionA" placeholder="Option A" required="true"><%=rs1.getString(4)%></textarea></td>
                </tr>
                <tr>
                    <td><strong><label><input type="radio" value="B" name="ans" required>B</label></strong></td>
                    <td><textarea name="optionB" class="col-md-12" id="optionB" placeholder="Option B" required="true"><%=rs1.getString(5)%></textarea></td>
                </tr>
                <tr>
                    <td><strong><label><input type="radio" value="C" name="ans" required>C</label></strong></td>
                    <td><textarea name="optionC" class="col-md-12" id="optionC" placeholder="Option C" required="true"><%=rs1.getString(6)%></textarea></td>
                </tr>
                <tr>
                    <td><strong><label><input type="radio" value="D" name="ans" required>D</label></strong></td>
                    <td><textarea name="optionD" class="col-md-12" id="optionD" placeholder="Option D" required="true"><%=rs1.getString(7)%></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td><button type="reset" class="btn btn-primary " >Reset</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary btn-success">Submit</button></td>
                </tr>
            </table>
</div>
</form>                    
<%
            }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    }
%>