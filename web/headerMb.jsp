<%-- 
    Document   : headerMb
    Created on : Dec 19, 2017, 12:49:49 AM
    Author     : SHUBHAM
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:700,400&subset=cyrillic,latin,greek,vietnamese">
  <link rel="stylesheet" href="assets/mobirise/css/style.css">
  <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
  <script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smooth-scroll/smooth-scroll.js"></script>
  <script src="assets/mobirise/js/script.js"></script>
  <nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="197">
  <section class="mbr-navbar mbr-navbar-freeze mbr-navbar-absolute mbr-navbar--auto-collapse" id="ext_menu-12" data-rv-view="33">
    <div class="mbr-navbar__section mbr-section">
        <div class="mbr-section__container container">
            <div class="mbr-navbar__container">
                <div class="mbr-navbar__column mbr-navbar__column--s mbr-navbar__brand">
                    <span class="mbr-navbar__brand-link mbr-brand mbr-brand--inline">
                        <span class="mbr-brand__logo"><a href="#">
                            <img src="assets/images/mbr-454x256.jpg" class="mbr-navbar__brand-img mbr-brand__img"></a>
                        </span>
                        <span class="mbr-brand__name">
                            <a class="mbr-brand__name text-white">Quiszz</a>
                        </span>
                    </span>
                </div>
                <div class="mbr-navbar__hamburger mbr-hamburger">
                    <span class="mbr-hamburger__line"></span>
                </div>
                <div class="mbr-navbar__column mbr-navbar__menu">
                    <nav class="mbr-navbar__menu-box mbr-navbar__menu-box--inline-right">
                        <div class="mbr-navbar__column">
                            <ul class="mbr-navbar__items mbr-navbar__items--right float-left mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-decorator mbr-buttons--active">
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white">HOME</a></li>
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white">ABOUT</a></li>  
                            </ul>                            
                            <ul class="mbr-navbar__items mbr-navbar__items--right mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-inverse mbr-buttons--active">
                                <li class="mbr-navbar__item"><a class="mbr-buttons__btn btn btn-danger" href="logout.jsp">Logout!</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
      </nav>
    <section class="engine"></section>
   