<%-- 
    Document   : adminHome
    Created on : Nov 28, 2017, 7:18:56 PM
    Author     : SHUBHAM
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.EditDAO"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="db.DBConnector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Master Home</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <link rel="shortcut icon" href="assets/ico/qs-logo.png">
    </head>     
        <%
            Statement st=DBConnector.getStatement();
            String username=(String)session.getAttribute("username");
            if(username==null || username.trim().equals(""))
            {
                response.sendRedirect("adminlogin.html");
            }
        %>
      <body onload="login()">
          <style>
            table {
            overflow: scroll;
            }
        </style>
          <%
              ArrayList<Integer> res = new ArrayList<>();
          %>
        <script>
        function login()
                {
                    alert("Login Success...");
                }
/****************************************************************************************************/                
/*******************************************Table Search*********************************************/
            (function(document) {
            'use strict';
            var LightTableFilter = (function(Arr) {
		var _input;
		function _onInputEvent(e) {
			_input = e.target;
			var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
			Arr.forEach.call(tables, function(table) {
				Arr.forEach.call(table.tBodies, function(tbody) {
					Arr.forEach.call(tbody.rows, _filter);
				});
			});
		}
		function _filter(row) {
			var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
			row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
		}
		return {
			init: function() {
				var inputs = document.getElementsByClassName('light-table-filter');
				Arr.forEach.call(inputs, function(input) {
					input.oninput = _onInputEvent;
				});
			}
		};
            })(Array.prototype);
            document.addEventListener('readystatechange', function() {
		if (document.readyState === 'complete') {
			LightTableFilter.init();
		}
            });
})(document);
/***************************************Table Search*********************************************/
/************************************************************************************************/        
        function setCheckboxStudent(c) 
            {
                if (c.checked) 
		{
                    document.getElementById('isSelectedStudent').value="checked";
		}	 
		else 
		{
		    document.getElementById('isSelectedStudent').value="unchecked";
		}
            }
        function setCheckboxSubject(cS) 
            {
                if (cS.checked) 
		{
                    document.getElementById('isSelectedSubject').value="checked";
		}	 
		else 
		{
		    document.getElementById('isSelectedSubject').value="unchecked";
		}
            }
/***********************************Sorted Report******************************************************/
    function braSet() 
    {
        document.getElementById('enrS').value="";
        document.getElementById('aboutS').value="branch";
    }
    function enSet() 
    {
        document.getElementById('branchSet').value="";
        document.getElementById('aboutS').value="enrollment";
    }
    
/***********************************Sorted Report******************************************************/
        </script>
        <style>
        .responsive {
            width: 100%;
            max-width: 650px;
            height: auto;
        }
    </style>
        <!--Header-->
  <%@include file="header.jsp"%>
  <script type="text/javascript" src="findstudent.js"></script>
  <script type="text/javascript" src="findfaculty.js"></script>
  <script type="text/javascript" src="tablesearch.js"></script>
  <script type="text/javascript" src="authfaculty.js"></script>
  <script type="text/javascript" src="checkBox.js"></script>
<div class="container well">
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
    <!--<li><a data-toggle="tab" href="#profile">PROFILE</a></li>-->
    <li><a data-toggle="tab" href="#viewstudent">View Student</a></li>
    <li><a data-toggle="tab" href="#viewfaculty">View Faculty</a></li>
    <li><a data-toggle="tab" href="#AuthenticateFaculty">Authenticate Faculty</a></li>
    <li><a data-toggle="tab" href="#viewsubjects">View Subjects</a></li>
    <li><a data-toggle="tab" href="#viewsetup">Quiz Setup</a></li>
    <li><a data-toggle="tab" href="#viewresult">View Result</a></li>
    <li><a data-toggle="tab" href="#changepassword">Change Password</a></li>
  </ul>
  <br>
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <div class="span10">
          <center><img src="images/center_box_bg.gif" alt="Online Exam System" class="responsive"/>  </center>
        <p style="font-weight: bold;font-size:20px;color:#808080;line-height: 25px;">
            <bold style="color:black;">Online Quiz</bold>
                <dl>
                    <dt>Features :</dt>
                    <dd>- Time Saver.</dd>
                    <dd>- Help the students to develop their skills.</dd>
                    <dd>- Randomized Question.</dd>
                    <dd>- Add different Subjects.</dd>
                    <dd>- Customized the Quiz.</dd>
                    <dd>- Edit or delete questions.</dd>
                    <dd>- View Result instantly.</dd>
                    <dd>- Generate Result Report.</dd>
                </dl>
        </p>
     </div>
    </div>
    <div id="profile" class="tab-pane fade">
        <div class="container">
            <div class="card" style="width:250px">
                <img class="card-img-top" src="images/mr_Q.png" alt="Card image" style="width:100%">
                <div class="card-body">
                    <a href="#" class="btn btn-primary">See Profile</a>
                </div>
            </div>
        </div>
    </div>
    <div id="viewstudent" class="tab-pane fade">
      <h3>Student Record :</h3>
      
      <%
            try
            {
            //Statement st1=DBConnector.getStatement();
            String query2="Select * from newuser";
            ResultSet rs4=st.executeQuery(query2);
      %>
      <span>
       <div class="col-md-3">
           <div class="input-group">
            <input class="form-control" type="text" id="myInput" onkeyup="myFunction()" placeholder="Search Id..." title="Type in a name">
        <div class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
        </div>
            </div>
       </div>
        <div class="col-md-3">
           <div class="input-group">
            <input class="form-control" type="text" id="myInputName" onkeyup="myFunctionName()" placeholder="Search Name..." title="Type in a name">
        <div class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
        </div>
            </div>
       </div>
          <div class="col-md-3">
           <div class="input-group">
            <input class="form-control" type="text" id="myInputBranch" onkeyup="myFunctionBranch()" placeholder="Search Branch..." title="Type in a name">
        <div class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
        </div>
            </div>
       </div>
          <div class="col-md-3">
           <div class="input-group">
            <input class="form-control" type="text" id="myInputSession" onkeyup="myFunctionSession()" placeholder="Search Session..." title="Type in a name">
        <div class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
        </div>
            </div>
       </div>
       </span>
      <form action="DeleteStudent" method="post" onsubmit="return confirm('Are you sure ?');">   
          <input hidden type="text" id="isSelectedStudent" name="isSelectedStudent" value="unchecked"/>
  <table style="padding-top: 20px;" class="table table-responsive table-bordered table-hover"  id="myTable"> 
    <tbody>
      <tr class="header">
        <td><strong>Enrollment :</strong></td>
        <td><strong>Name :</strong></td>
        <td><strong>Email :</strong></td>
        <td><strong>Phone No. :</strong></td>
        <td><strong>DOB :</strong></td>
        <td><strong>Gender :</strong></td>
        <td><strong>Branch :</strong></td>
        <td><strong>Session :</strong></td>
        <td><input type="checkbox" id="selectallStudent"/></td>
      </tr>
        <%  
             while(rs4.next())
               {        
        %>
        <tr> 
          <td><%= rs4.getString(2)%></td>
          <td><%= rs4.getString(1)%></td>
          <td><%= rs4.getString(3)%></td>
          <td><%= rs4.getString(4)%></td>
          <td><%= rs4.getString(5)%></td>
          <td><%= rs4.getString(6)%></td>
          <td><%= rs4.getString(7)%></td>
          <td><%= rs4.getString(8)%></td>
          <td><input type="checkbox" class="deleteS" id="deleteS" name="deleteS" onchange="setCheckboxStudent(this)" value=<%=rs4.getString(2)%>></td>
        </tr>
        <%      }
             }
             catch(SQLException e)
             {
                System.out.println(e);
             } 
        %>
    </tbody>
  </table>
    <div style="padding-top: 20px;">
        <button name="submit" type="submit" class="btn btn-danger" style="width:90px;height:40px;margin:auto;display:block"><span class="glyphicon glyphicon-trash"></span> Delete</button>
    </div><br><br><br>
</form>
    </div>
    <div id="viewfaculty" class="tab-pane fade">
      <h3>Faculty Record :</h3>
     <%
            try
            {
            //Statement st1=DBConnector.getStatement();
            String query2="Select * from FACULTY";
            ResultSet rs3=st.executeQuery(query2);
      %>
       <input type="text" class="form-control light-table-filter" id="myInputFaculty" data-table="order-table" placeholder="Search Id..." title="Type in a name">
      <form action="DeleteStudent" method="post">   
  <table class="order-table table table-responsive table-bordered table-hover"  id="myTableFaculty"> 
    <tbody>
      <tr class="header">
        <td><strong>Faculty ID :</strong></td>
        <td><strong>Name :</strong></td>
        <td><strong>Email :</strong></td>
        <td><strong>Phone No. :</strong></td>
        <td><strong>Account Status :</strong></td>
      </tr>
        <%  
             String fla="";
             while(rs3.next())
               { 
               int flag=rs3.getInt(8);
               if(flag>0)
               {
                   fla="Active";
               }
               else
               {
                   fla="Deactive";
               }
        %>          
        <tr> 
          <td><%= rs3.getString(2)%></td>
          <td><%= rs3.getString(1)%></td>
          <td><%= rs3.getString(3)%></td>
          <td><%= rs3.getString(4)%></td>
          <td><%= fla%></td>
        </tr>
        <%      }
             }
             catch(SQLException e)
             {
                System.out.println(e);
             } 
        %>
    </tbody>
  </table>
</form> 
    </div>
    <div id="AuthenticateFaculty" class="tab-pane fade">
      <h3>Authenticate Faculty :</h3>
      <form name="regF" action="#" method="get">
          <input type="text" name="facultyid" class="form-control" placeholder="Search.." onkeyup="sendFacultyInfoA()">
      </form>
      <div id="facultyauth">
          
      </div>
    </div>
    <div id="viewsubjects" class="tab-pane fade">
      <%
            try
            {
            //Statement st1=DBConnector.getStatement();
            String query2="Select * from addsubject";
            ResultSet rs2=st.executeQuery(query2);
      %>
<form action="DeleteSubject" onsubmit="return confirm('Are you sure ?');" method="post">  
    <input hidden type="text" id="isSelectedSubject" name="isSelectedSubject" onchange="setCheckboxSubject(this)" value="unchecked"/>
  <table class="table table-responsive table-bordered table-hover"> 
    <tbody>
      <tr>
        <td><strong>Subject Code :</strong></td>
        <td><strong>Subject Name :</strong></td>
        <td><strong>Check Box :<input type="checkbox" id="selectall"/></strong></td>
      </tr>
        <%
             while(rs2.next())
               { 
        %>          
        <tr> 
          <td><%= rs2.getString(1)%></td>
          <td><%= rs2.getString(2)%></td>
          <td><input type="checkbox" class="delete" name="delete" id="delete" onchange="setCheckboxSubject(this)" value=<%=rs2.getString(1)%>></td>
        </tr>
        <%      }
             }
             catch(SQLException e)
             {
                System.out.println(e);
             } 
        %>
        <tr> 
          <td></td>
          <td></td>
          <td><button type="submit" name="submit" class="btn btn-danger "><span class="glyphicon glyphicon-trash"></span> Delete</button></td>
        </tr>
    </tbody>
  </table>
</form>
    </div>
    
    <div id="viewsetup" class="tab-pane fade">
        <table class="table table-striped table-responsive">
            <tbody>
                <tr>
                    <td><strong>Subject Code</strong></td>
                    <td><strong>Quiz Name</strong></td>
                    <td><strong>Total question</strong></td>
                    <td><strong>Question Level</strong></td>
                    <td><strong>Marks for each</strong></td>
                    <td><strong>Negative Marks</strong></td>
                    <td><strong>Time Duration</strong></td>
                    <td><strong>Faculty</strong></td>
                </tr>
    <%
        try
        {
            Statement stvs=DBConnector.getStatement();
            ResultSet rsvs=stvs.executeQuery("Select * from quizsetup");
            while(rsvs.next())
            {
    %>          <tr>
                    <td><%=rsvs.getString(1)%></td>
                    <td><%=rsvs.getString(2)%></td>
                    <td><%=rsvs.getString(3)%></td>
                    <td><%=rsvs.getString(10)%></td>
                    <td><%=rsvs.getString(4)%></td>
                    <td><%=rsvs.getString(5)%></td>
                    <td><%=rsvs.getString(6)%></td>
                    <td><%=rsvs.getString(7)%></td>
                </tr>
    <%
            }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    %>            
            </tbody>
        </table>        
    </div>    
        
    <div id="viewresult" class="tab-pane fade">
        <br>
        <span>
       <div class="col-md-3">
           <div class="input-group">
            <input class="form-control" type="text" id="myInputSCode" onkeyup="myFunctionSCode()" placeholder="Search Subject Code..." title="Type in a name">
        <div class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
        </div>
            </div>
       </div>
        <div class="col-md-3">
           <div class="input-group">
            <input class="form-control" type="text" id="myInputEName" onkeyup="myFunctionEName()" placeholder="Search Exam Name..." title="Type in a name">
        <div class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
        </div>
            </div>
       </div>
          <div class="col-md-3">
           <div class="input-group">
            <input class="form-control" type="text" id="myInputEnroll" onkeyup="myFunctionEnroll()" placeholder="Search Enrollment no..." title="Type in a name">
        <div class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
        </div>
            </div>
       </div>
       </span>
        <br>
        <table class="table table-responsive table-bordered table-hover" id="myTableResult">
            <tbody>
                <tr>
                  <td><strong>Subject Code :</strong></td>  
                  <td><strong>Exam Name :</strong></td>  
                  <td><strong>Enrollment :</strong></td>
                  <td><strong>Score :</strong></td>
                  <td><strong>Correct Attempts :</strong></td>
                  <td><strong>Wrong Attempts :</strong></td>
                </tr>
        <%
            try
            {
                ResultSet rsR=null;
                rsR=st.executeQuery("Select * from result");
                while(rsR.next())
                { 
                    res.add(8);
                %>
                <tr>
                  <td><%=rsR.getString(8)%></td>  
                  <td><%=rsR.getString(7)%></td>  
                  <td><%=rsR.getString(3)%></td>
                  <td><%=rsR.getString(4)%></td>
                  <td><%=rsR.getString(5)%></td>
                  <td><%=rsR.getString(6)%></td>
                </tr>
                <%    
                }
            }
            catch(Exception e)
            {
                System.out.println(e);
            }
        %>
            </tbody>
        </table>
<!--/****************************************Report Generation***********************************************************/-->           
            <form action="Report" method="post" onsubmit="return confirm('Are you sure ?');">
                <input hidden type="text" value="complete" name="about"/>
                <input hidden type="text" value="<%=username%>" name="user">
                <div style="padding-top: 30px;">
        <button type="submit" class="btn btn-warning" style="width:200px;height:50px;margin:auto;display:block">Generate Result Report!</button>
                </div><br>
            </form>
        <a href="#" data-toggle="modal" style="width:150px;height:25px;margin:auto;display:block" data-target="#filterReport">Filtered Report</a><br><br><br><br>
    <!--/*************************************Report Modal********************************************/-->     

<div id="filterReport" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select Any one of the Options</h4>
      </div>
        
      <div class="modal-body">
          <form name="regS" action="Report" method="post" onsubmit="return confirm('Are you sure ?');">
              <input hidden type="text" name="about" id="aboutS" value=""/>
          <table class="table table-bordered table-condensed">
              <tbody>
                  <tr>
                      <td><strong>Enrollment</strong></td>
                      <td><strong>Branch</strong></td>
                  </tr>
                  <tr>
                      <td><input class="form-control" type="text" name="enrollment" id="enrS" onkeyup="enSet();sendStudentInfo();"></td>
                      <td><select class="form-control" name="branchSet" id="branchSet" onchange="braSet()">
                              <option value="">--Select Branch--</option>
                              <option value="IT">IT</option>
                              <option value="CS">CS</option>
                              <option value="EC">EC</option>
                              <option value="EI">EI</option>
                              <option value="CE">CE</option>
                              <option value="ME">ME</option>
                              <option value="AU">AU</option>
                          </select>
                      </td>
                  </tr>
                  <tr>
                    <td><button type="reset" class="btn btn-primary " data-loading-text="Loading...">Reset</button></td>
                    <td><button type="submit" class="btn btn-primary btn-success" data-loading-text="Loading...">Submit</button></td>
                  </tr>
              </tbody>
          </table>
              <div id="studentcheck">
                  <!--Student Check-->
              </div>
          </form>
          
          
      </div>
        
      <div class="modal-footer">
          <button type="button" class="btn btn-default btn-block" data-dismiss="modal"><span class="glyphicon glyphicon-cloud"></span> Close</button>
      </div>
        
    </div>
  </div>
</div>
    <!--/*************************************Report Modal********************************************/-->
            </form> 
<!--/****************************************Report Generation***********************************************************/-->    </div>
            
            
     <div id="changepassword" class="tab-pane fade">
              <div class="col-md-8 col-md-offset-2">
        <form action="AdminPassChange" method="post" onsubmit="return confirm('Are you sure ?');"> 
   <table class="table table-responsive table-bordered table-hover"> 
    <tbody>
      <tr>
        <td><strong>Name :</strong></td>
        <td><%=username%><input hidden type="text" name="username" value="<%=username%>" readonly /></td>
      </tr>
      <tr>
        <td><strong>Current Password :</strong></td>
        <td><input type="password" name="password" placeholder="******" required/></td>
      </tr>
      <!--
      <tr>
        <td><strong>Retype Current Password :</strong></td>
        <td><input type="password" name="retypepassword" placeholder="******" required /></td>
      </tr>-->
      <tr>
        <td><strong>New Password :</strong></td>
        <td><input type="password" name="newpassword" placeholder="******" required /></td>
      </tr>
      <tr>
        <td><button type="reset" class="btn btn-primary " data-loading-text="Loading...">Reset</button></td>
        <td><button type="submit" class="btn btn-primary btn-success" data-loading-text="Loading...">Submit</button></td>
      </tr>
    </tbody>
  </table>
      </form>
      </div>
     </div> 
  </div>
</div>
            <!--Footer-->
        <%@include file="footer.jsp"%>
    </body>
</html>
