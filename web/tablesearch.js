/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var input, filter, table, tr, td, i;

function myFunction() 
{
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) 
  {
    td = tr[i].getElementsByTagName("td")[0];  //row search(Whether enroll,branch,name...)
    if (td) 
    {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) 
      {
        tr[i].style.display = "";
      } 
      else 
      {
        tr[i].style.display = "none";
      }
    }       
  }
}

function myFunctionName() 
{
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInputName");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) 
  {
    td = tr[i].getElementsByTagName("td")[1];  //row search(Whether enroll,branch,name...)
    if (td) 
    {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) 
      {
        tr[i].style.display = "";
      } 
      else 
      {
        tr[i].style.display = "none";
      }
    }       
  }
}

function myFunctionBranch() 
{
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInputBranch");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) 
  {
    td = tr[i].getElementsByTagName("td")[6];  //row search(Whether enroll,branch,name...)
    if (td) 
    {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) 
      {
        tr[i].style.display = "";
      } 
      else 
      {
        tr[i].style.display = "none";
      }
    }       
  }
}

function myFunctionSession() 
{
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInputSession");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) 
  {
    td = tr[i].getElementsByTagName("td")[7];  //row search(Whether enroll,branch,name...)
    if (td) 
    {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) 
      {
        tr[i].style.display = "";
      } 
      else 
      {
        tr[i].style.display = "none";
      }
    }       
  }
}


//---------------------------------------------Result-----------------------------------------------------/

var inputR, filterR, tableR, trR, tdR, i;
function myFunctionSCode() 
{
  var inputR, filterR, tableR, trR, tdR, i;
  inputR = document.getElementById("myInputSCode");
  filterR = inputR.value.toUpperCase();
  tableR = document.getElementById("myTableResult");
  trR = tableR.getElementsByTagName("tr");
  for (i = 0; i < trR.length; i++) 
  {
    tdR = trR[i].getElementsByTagName("td")[0];  //row search(Whether enroll,branch,name...)
    if (tdR) 
    {
      if (tdR.innerHTML.toUpperCase().indexOf(filterR) > -1) 
      {
        trR[i].style.display = "";
      } 
      else 
      {
        trR[i].style.display = "none";
      }
    }       
  }
}

function myFunctionEName() 
{
  var inputR, filterR, tableR, trR, tdR, i;
  inputR = document.getElementById("myInputEName");
  filterR = inputR.value.toUpperCase();
  tableR = document.getElementById("myTableResult");
  trR = tableR.getElementsByTagName("tr");
  for (i = 0; i < trR.length; i++) 
  {
    tdR = trR[i].getElementsByTagName("td")[1];  //row search(Whether enroll,branch,name...)
    if (tdR) 
    {
      if (tdR.innerHTML.toUpperCase().indexOf(filterR) > -1) 
      {
        trR[i].style.display = "";
      } 
      else 
      {
        trR[i].style.display = "none";
      }
    }       
  }
}

function myFunctionEnroll() 
{
  var inputR, filterR, tableR, trR, tdR, i;
  inputR = document.getElementById("myInputEnroll");
  filterR = inputR.value.toUpperCase();
  tableR = document.getElementById("myTableResult");
  trR = tableR.getElementsByTagName("tr");
  for (i = 0; i < trR.length; i++) 
  {
    tdR = trR[i].getElementsByTagName("td")[2];  //row search(Whether enroll,branch,name...)
    if (tdR) 
    {
      if (tdR.innerHTML.toUpperCase().indexOf(filterR) > -1) 
      {
        trR[i].style.display = "";
      } 
      else 
      {
        trR[i].style.display = "none";
      }
    }       
  }
}