<%-- 
    Document   : home
    Created on : Nov 26, 2017, 1:44:29 PM
    Author     : SHUBHAM
--%>

<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="db.DBConnector"%>
<%@page import="java.sql.Statement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
     <head>
        <title>Student Home</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="assets/ico/qs-logo.png">
    </head>
    <body onload="login()">
        
        <%@include file ="header.jsp"%>
        <%
            String name="",email="",pno="",dob="",gen="",branch="",sess="";
            String enrollment=(String)session.getAttribute("enrollment");
            if(enrollment==null || enrollment.trim().equals(""))
            {
                response.sendRedirect("login.html");
            }
            try
            {
            Statement st=DBConnector.getStatement();
            String query="Select * from newUser where enrollment='"+enrollment+"'";
            //System.out.println(query);
            ResultSet rs=st.executeQuery(query);
            while(rs.next())
                {
                    name=rs.getString(1);
                    email=rs.getString(3);
                    pno=rs.getString(4);
                    dob=rs.getString(5);
                    gen=rs.getString(6);
                    branch=rs.getString(7);
                    sess=rs.getString(8);
                }
            }
        catch(SQLException e)
        {
            System.out.println(e);
        }
        %>
            <script>
                function login()
                {
                    alert("Login Success...");
                }
                var requestA;
                    function sendSubjectInfoQ()
                    {
                        var v = document.quiz.subject.value;
                        var url = "quizst1.jsp?val=" + v +"&enroll=" + '<%=enrollment%>';
                        if (window.XMLHttpRequest)
                        {
                            requestA = new XMLHttpRequest();
                        } else if (window.ActiveXObject)
                        {
                            requestA = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        try
                        {
                            requestA.onreadystatechange = getSubjectInfoQ;
                            requestA.open("GET", url, true);
                            requestA.send();
                        } catch (e)
                        {
                            alert("Unable to connect to server");
                        }
                    }
                    function getSubjectInfoQ()
                    {
                        if (requestA.readyState == 4)
                        {
                            var val = requestA.responseText;
                            document.getElementById('setup').innerHTML = val;
                        }
                    }
            </script>
      <style>
        .responsive {
            width: 100%;
            max-width: 650px;
            height: auto;
        }
    </style>
       <div class="container well">
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
    <li><a data-toggle="tab" href="#profile">PROFILE</a></li>
    <li><a data-toggle="tab" href="#viewsubjects">View Subjects</a></li>
    <li><a data-toggle="tab" href="#exam">Exam</a></li>
    <li><a data-toggle="tab" href="#viewresult">View Result</a></li>
    <li><a data-toggle="tab" href="#changepassword">Change Password</a></li>
  </ul>
  <br>
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <div class="span10">
          <center>    <img src="images/center_box_bg.gif" alt="Online Exam System" class="responsive"/>  </center>
        <p style="font-weight: bold;font-size:20px;color:#808080;line-height: 25px;">
            <bold style="color:black;">Online Quiz</bold>
                <dl>
                   <dt>Features :</dt>
                    <dd>- Time Saver.</dd>
                    <dd>- Help the students to develop their skills.</dd>
                    <dd>-  Randomized Question.</dd>
                    <dd>-  View Result instantly.</dd>
                </dl>
        </p>
       </div>
    </div>
    <div id="profile" class="tab-pane fade">
        <div class="container">       
  <div class="col-md-4 col-md-offset-4">
      <h1>Personal Details :</h1>
  <table class="table table-responsive table-bordered table-hover"> 
    <tbody>
      <tr>
          <td><strong>Name :</strong></td>
        <td><%=name%></td>
      </tr>
      <tr>
        <td><strong>Enrollment No :</strong></td>
        <td><%=enrollment%></td>
      </tr>
      <tr>
        <td><strong>Branch :</strong></td>
        <td><%=branch%></td>
      </tr>
      <tr>
        <td><strong>Session :</strong></td>
        <td><%=sess%></td>
      </tr>
      <tr>
        <td><strong>Email :</strong></td>
        <td><%=email%></td>
      </tr>
      <tr>
        <td><strong>Phone Number :</strong></td>
        <td><%=pno%></td>
      </tr>
      <tr>
        <td><strong>DOB :</strong></td>
        <td><%=dob%></td>
      </tr>
      <tr>
        <td><strong>Gender :</strong></td>
        <td><%=gen%></td>
      </tr>
    </tbody>
  </table>
      </div>
</div>
    </div>
      
    <div id="exam" class="tab-pane fade">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
         <strong><h1>Quiz</h1></strong>
         <form name="quiz">
        <div class="form-group">
            <div class="selectContainer">
                <select class="form-control" name="subject" onchange="sendSubjectInfoQ()" required>
                    <%
                        try{
                        Statement st=DBConnector.getStatement();
                        ResultSet rs2=st.executeQuery("Select * from addsubject");
                    %>
                    <option value="">--Select Subject--</option>
                    <%
                        while(rs2.next())
                    {
                    %>
                    <option value="<%=rs2.getString(1)%>"><%=rs2.getString(1)%> : <%=rs2.getString(2)%></option>
                    <%      }
                }
            catch(SQLException e)
            {
                System.out.println(e);
            }
        %>
                </select>
            </div>
        </div>
        </form>
                <div id="setup">
                    
                </div>
        </div>
      </div>
    </div>
      
    <div id="viewsubjects" class="tab-pane fade">
       <%
            try
            {
            Statement st1=DBConnector.getStatement();
            String query2="Select * from addsubject";
            ResultSet rs2=st1.executeQuery(query2);
        %>
   <table class="table table-responsive table-bordered table-hover"> 
    <tbody>
      <tr>
        <td><strong>Subject Code :</strong></td>
        <td><strong>Subject Name :</strong></td>
      </tr>
        <%
             while(rs2.next())
               {
        %>
        <tr> 
          <td><%= rs2.getString(1)%></td>
          <td><%= rs2.getString(2)%></td>
        </tr>
        <%      }
             }
             catch(SQLException e)
             {
                System.out.println(e);
             } 
        %>
    </tbody>
  </table>
    </div>
    <div id="viewresult" class="tab-pane fade">
        <table class="table table-responsive table-bordered table-hover">
            <tbody>
                <tr>
                  <td><strong>Subject Code :</strong></td>
                  <td><strong>Quiz Name :</strong></td>
                  <td><strong>Score :</strong></td>
                  <td><strong>Correct Attempts :</strong></td>
                  <td><strong>Wrong Attempts :</strong></td>
                </tr>
        <%
            try
            {
                Statement st3=DBConnector.getStatement();
                Statement st4=DBConnector.getStatement();
                ResultSet rs3=null,rs4=null;
                rs3=st3.executeQuery("Select * from result where enrollment='"+enrollment+"'");
                while(rs3.next())
                {   
                    %>
                <tr>
                  <td><%=rs3.getString(8)%></td>
                  <td><%=rs3.getString(7)%></td>
                  <td><%=rs3.getString(4)%></td>
                  <td><%=rs3.getString(5)%></td>
                  <td><%=rs3.getString(6)%></td>
                </tr>        
                    <%
                }
            }
            catch(Exception e)
            {
                System.out.println(e);
            }
        %>
            </tbody>
        </table>
    </div>
     <div id="changepassword" class="tab-pane fade">
    <div class="col-md-8 col-md-offset-2">
        <form action="StudentPassChange" method="post"> 
   <table class="table table-responsive table-bordered table-hover"> 
    <tbody>
      <tr>
        <td><strong>Enrollment No :</strong></td>
        <td><input type="text" name="enrollment" value="<%=enrollment%>" readonly /></td>
      </tr>
      <tr>
        <td><strong>Current Password :</strong></td>
        <td><input type="password" name="password" placeholder="******" required/></td>
      </tr>
      <!--
      <tr>
        <td><strong>Retype Current Password :</strong></td>
        <td><input type="password" name="retypepassword" placeholder="******" required /></td>
      </tr>-->
      <tr>
        <td><strong>New Password :</strong></td>
        <td><input type="password" name="newpassword" placeholder="******" required /></td>
      </tr>
      <tr>
        <td><button type="reset" class="btn btn-primary " data-loading-text="Loading...">Reset</button></td>
        <td><button type="submit" class="btn btn-primary btn-success" data-loading-text="Loading...">Submit</button></td>
      </tr>
    </tbody>
  </table>
      </form>
      </div></div>
  </div>
</div>
        <!--Footer-->
        <%@include file="footer.jsp"%>
    </body>
</html>
