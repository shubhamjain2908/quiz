<%-- 
    Document   : header
    Created on : Dec 2, 2017, 5:15:29 PM
    Author     : SHUBHAM
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
     <nav class="navbar navbar-inverse" style="border-bottom:8px solid #007AF4;">
    <div class="container-fluid">
        <a class="navbar-brand">
            <img src="images/quiz.jpg" alt="logo" style="width:40px;" class="responsive">
        </a>
        <!-- Logo -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavBar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">Quiz Show</a>
        </div>

        <!-- Menu Items -->
        <div class="collapse navbar-collapse" id="mainNavBar">
            <ul class="nav navbar-nav">
                <li><a href="#"><button type="button" onclick="myFunction()" class="btn btn-info"><span class="glyphicon glyphicon-home"></span> Home</button></a></li>
                <li><a href="#" data-toggle="modal" data-target="#aboutInfo"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-pencil"></span> About</button></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="logout.jsp"><button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-log-out"></span> Logout</button></a></li>
            </ul>
        </div>

    </div>
</nav>
    <!--/*************************************About Modal********************************************/-->     

<div id="aboutInfo" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><strong>Application Features : </strong></h4>
      </div>
        
      <div class="modal-body">
          <div align="center" style='color:#808080; font-weight: bold;' class="modal-body">
                <ul class="list-group">
                    <li class="list-group-item list-group-item-success">Time Saver.</li>
                    <li class="list-group-item list-group-item-info">Help the students to develop their skills.</li>
                    <li class="list-group-item list-group-item-warning">Add different Subjects.</li>
                    <li class="list-group-item list-group-item-danger">Add Questions.</li>
                    <li class="list-group-item list-group-item-success">Edit or delete questions.</li>
                    <li class="list-group-item list-group-item-info">Customized the Quiz.</li>
                    <li class="list-group-item list-group-item-warning">Randomized Question.</li>
                    <li class="list-group-item list-group-item-danger">Customized the Quiz.</li>
                    <li class="list-group-item list-group-item-success">View Result instantly.</li>
                    <li class="list-group-item list-group-item-info">Generate Result Report.</li>
                </ul>
            </div>
      </div>
        
      <div class="modal-footer">
          <button type="button" class="btn btn-default btn-block" data-dismiss="modal"><span class="glyphicon glyphicon-cloud"></span> Close</button>
      </div>
        
    </div>
  </div>
</div>
    <!--/*************************************About Modal********************************************/-->

<script type="text/javascript">
    function myFunction() 
    {
        location.reload();
    }
</script>