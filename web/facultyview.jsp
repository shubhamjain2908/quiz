<%-- 
    Document   : facultyview
    Created on : Dec 8, 2017, 12:34:59 AM
    Author     : SHUBHAM
--%>

<%@page import="java.sql.Statement"%>
<%@page import="db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <%
            String fid = "", fname = "", femail = "", fpno = "", fdob = "", fgen = "",flag1="";
            int flag;
           String n=request.getParameter("val");
		if(n.length()>0)
                {
                    try
                    {
                        Statement st=DBConnector.getStatement();
                        String query="Select * from faculty where facultyid='"+n+"'";
                        System.out.println(query);
                        ResultSet rs=st.executeQuery(query);
                        while (rs.next()) {
                        fid = rs.getString(2);
                        fname = rs.getString(1);
                        femail = rs.getString(3);
                        fpno = rs.getString(4);
                        fgen = rs.getString(6);
                        fdob = rs.getString(5);
                        flag = rs.getInt(8);
                        if(flag!=0)
                        {
                            flag1="Authorised Faculty".toString();
                            out.print("<font style='color:red'>Faculty Exist...!</font>");
                        }
                        else
                        {
                            flag1="Not Authorised".toString();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        %>  
        <div class="container">       
            <div class="col-md-4 col-md-offset-4">
                <h1>Personal Details :</h1>
                <table class="table table-responsive table-bordered table-hover"> 
                    <tbody>
                        <tr>
                            <td><strong>Name :</strong></td>
                            <td><%=fname%></td>
                        </tr>
                        <tr>
                            <td><strong>ID :</strong></td>
                            <td><%=n%></td>
                        </tr>
                        <tr>
                            <td><strong>Email :</strong></td>
                            <td><%=femail%></td>
                        </tr>
                        <tr>
                            <td><strong>Phone Number :</strong></td>
                            <td><%=fpno%></td>
                        </tr>
                        <tr>
                            <td><strong>DOB :</strong></td>
                            <td><%=fdob%></td>
                        </tr>
                        <tr>
                            <td><strong>Gender :</strong></td>
                            <td><%=fgen%></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><strong><%=flag1%></strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
