$(function(){

	// add multiple select / deselect functionality
	$("#selectall").click(function () {
		  $('.delete').attr('checked', this.checked);
	});

	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$(".delete").click(function(){

		if($(".delete").length == $(".delete:checked").length) {
			$("#selectall").attr("checked", "checked");
		} else {
			$("#selectall").removeAttr("checked");
		}

	});
});

$(function(){

	// add multiple select / deselect functionality
	$("#selectallStudent").click(function () {
		  $('.deleteS').attr('checked', this.checked);
	});

	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$(".deleteS").click(function(){

		if($(".deleteS").length == $(".deleteS:checked").length) {
			$("#selectallStudent").attr("checked", "checked");
		} else {
			$("#selectallStudent").removeAttr("checked");
		}

	});
});