<%-- 
    Document   : FirstP
    Created on : Jan 8, 2018, 5:01:13 PM
    Author     : SHUBHAM
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            session.invalidate();
            //response.sendRedirect("FirstP.jsp");
        %>
        <title>Quiz Show</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="assets/ico/qs-logo.png">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/main.css">    
    </head>
    <body background="images/tumblr_ogquinbyl31vsjcxvo2_r1_1280.gif" >
        <%
            //session.invalidate();
            //response.sendRedirect("FirstP.jsp");
        %>
    <script src="main.js"></script>
    <!--Header-->
        <div class="navbar">
            <div class="navbar-inner" style="background:#000;border-bottom:5px solid #007AF4;height:70px;">
                <div class="container">
                    <nav class="navbar">
                    <!-- Brand/logo -->
                        <a class="navbar-brand" href="#" >
                            <img src="images/quiz.jpg" alt="logo" style="width:40px;">
                        </a>
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavBar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a href="#" class="navbar-brand">Quiz Show</a>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    <div id="showcase-container">
        <div class="container" id="parent-block-layout">
            <div class="row">
                <div class="col-sm-4 zoomImg">
                    <h3 align="centre" style="font-family: cursive;color: #FF0000">Student</h3>
                    <a href="login.html">
                        <img src="images/Webalys-Kameleon.pics-Student-3.jpg" height="220" width="220">
                    </a>             
                </div>
                <div class="col-sm-4 zoomImg">                                    
                    <h3 align="centre" style="font-family: cursive;color: #FF0000">Faculty</h3>
                    <a href="facultylogin.html">
                        <img src="images/faculty-icon-300x300.png" height="220" width="220">
                    </a>
                </div>
                <div class="col-sm-4 zoomImg">
                    <h3 align="centre" style="font-family: cursive;color: #FF0000">Quiz Master</h3>   
                    <a href="adminlogin.html">
                        <img src="images/mr_Q.png" height="220" width="220">
                    </a>
                </div>
            </div>
        </div>
    </div>
        <br><br><br><br><br><br>
            <!--Footer-->
        <%@include file="footer.jsp"%>
    </body>
</html>
